/// Scanning macros for Micro compiler written for Purdue ECE 46800 Fall 2017.
/// JWM

use tokens::*;

use nom::{digit, alpha, is_alphanumeric};
use std::str::from_utf8;

/// Creates the list of tokens.
named!(pub tokenize<Vec<UToken>>,
       fold_many0!( get_token, Vec::new(), |mut acc: Vec<_>, token| {
           acc.push(token);
           acc
       }));

/// Selects between the token categories.
named!(pub get_token<UToken>,
       alt_complete!(
           scan_comment |
           scan_keyword |
           scan_operator |
           scan_float |
           scan_int |
           scan_string |
           scan_identifier
           )
       );


// Identifiers
// One letter, then any letter or number
/// Defines scan_identifier, which captures valid identifiers.
/// Returns Identifier("") if bytes cannot be parsed.
/// (should not happen).
named!(pub scan_identifier<UToken>,
       ws!(do_parse!(
               name: recognize!(preceded!(alpha,take_while!(is_alphanumeric))) >>
               
               ({
                   
                   let name_str = match from_utf8(name) {
                       Ok(literal) => literal.to_string(),
                       _ => "".to_string()
                   };
                   UToken::Identifier(name_str)
               })
               )
           )
       );

// IntLeterals
/// Defines scan_int, which captures ints.
/// Returns IntLiteral("") if bytes cannot be parsed.
/// (should not happen).
named!(pub scan_int<UToken>,
       ws!(do_parse!(
               raw: recognize!(preceded!(opt!(tag!("-")), digit)) >>

               (match from_utf8(raw) {
                   Ok(literal) => UToken::IntLiteral(literal.to_string()),
                   _ => UToken::IntLiteral("".to_string()),
               }))
           )
       );


// FloatLiterals
/// Defines scan_float, which captures a float.
/// Returns FloatLiteral("") if bytes cannot be parsed.
/// (Should not happen). Currently doesn't handle 'X.'
/// as a float.
named!(pub scan_float<UToken>,
       ws!(do_parse!(
            sign: opt!(tag!("-")) >>
            digits1: opt!(digit) >>
            tag!(".") >>
            digits2: digit >>
            
            ({
                let sign_str = match sign {
                    Some(_) => "-".to_string(),
                    _ => "".to_string(),
                };
                fn parse_num(in_str: &[u8]) -> String { 
                    match from_utf8(in_str) {
                        Ok(owned) => owned.to_string(),
                        _ => "".to_string(),
                    }
                };
                let digits1_str = match digits1 {
                    Some(literal) => parse_num(literal),
                    _ => "".to_string(),
                };
                let digits2_str = parse_num(digits2);

                UToken::FloatLiteral(sign_str + &digits1_str + "." + &digits2_str)
            })
        ))
       );

// StringLiterals
/// Defines scan_string, which captures a " delimited string.
/// Will return StringLiteral("") if the bytes cannot be
/// parsed to a String. (Should not happen since we're not
/// using Unicode strings as input).
named!(pub scan_string<UToken>,
       ws!(do_parse!(
               literal: recognize!(delimited!(tag!("\""), take_until!("\""), tag!("\""))) >>
               ( match from_utf8(literal) {
                   Ok(contents) => UToken::StringLiteral(contents.to_string()),
                    _ => UToken::StringLiteral("".to_string()),
               })
               )
           )
       );

// Comments
/// Defines function scan_comment that captures a comment line and
/// returns a Comment token.
named!(pub scan_comment<UToken>,
       do_parse!(
           delimited!(ws!(tag!("--")), take_until!("\n"), tag!("\n")) >>
           
           (UToken::Comment)
           )
       );

// Keywords
/// A macro that defines a nom tag capture for a keyword or an operator.
macro_rules! make_enum_token {
    ($out_type:ty, $name:ident, $map_name:ident, $literal:expr, $return_val:expr) => (
        fn $map_name(_: &[u8]) -> Result<$out_type, ()> {
            Ok($return_val)
        }
        
        named!(pub $name<$out_type>,
               ws!(map_res!(tag!($literal), $map_name))
               );
    )
}

// Generate nom parsers that return Result<UTKeyword, _> for each keyword.
make_enum_token!(UTKeyword,
                 key_program,
                 map_program,
                 "PROGRAM",
                 UTKeyword::Program);
make_enum_token!(UTKeyword, key_begin, map_begin, "BEGIN", UTKeyword::Begin);
make_enum_token!(UTKeyword, key_end, map_end, "END", UTKeyword::End);
make_enum_token!(UTKeyword,
                 key_function,
                 map_function,
                 "FUNCTION",
                 UTKeyword::Function);
make_enum_token!(UTKeyword, key_read, map_read, "READ", UTKeyword::Read);
make_enum_token!(UTKeyword, key_write, map_write, "WRITE", UTKeyword::Write);
make_enum_token!(UTKeyword, key_if, map_if, "IF", UTKeyword::If);
make_enum_token!(UTKeyword, key_else, map_else, "ELSE", UTKeyword::Else);
make_enum_token!(UTKeyword, key_fi, map_fi, "FI", UTKeyword::Fi);
make_enum_token!(UTKeyword, key_for, map_for, "FOR", UTKeyword::For);
make_enum_token!(UTKeyword, key_rof, map_rof, "ROF", UTKeyword::Rof);
make_enum_token!(UTKeyword,
                 key_return,
                 map_return,
                 "RETURN",
                 UTKeyword::Return);
make_enum_token!(UTKeyword, key_int, map_int, "INT", UTKeyword::Int);
make_enum_token!(UTKeyword, key_void, map_void, "VOID", UTKeyword::Void);
make_enum_token!(UTKeyword,
                 key_string,
                 map_string,
                 "STRING",
                 UTKeyword::String_);
make_enum_token!(UTKeyword, key_float, map_float, "FLOAT", UTKeyword::Float);

/// Select between keywords
named!(pub scan_keyword<UToken>,
       alt!(
           map!(key_program, UToken::Keyword) |
           map!(key_begin, UToken::Keyword) |
           map!(key_end, UToken::Keyword) |
           map!(key_function, UToken::Keyword) |
           map!(key_read, UToken::Keyword) |
           map!(key_write, UToken::Keyword) |
           map!(key_if, UToken::Keyword) |
           map!(key_else, UToken::Keyword) |
           map!(key_fi, UToken::Keyword) |
           map!(key_for, UToken::Keyword) |
           map!(key_rof, UToken::Keyword) |
           map!(key_return, UToken::Keyword) |
           map!(key_int, UToken::Keyword) |
           map!(key_void, UToken::Keyword) |
           map!(key_string, UToken::Keyword) |
           map!(key_float, UToken::Keyword)
           )
       );

// Operators
// Generate nom parsers that return Result<UTOperator, _> for each operator.
make_enum_token!(UTOperator, op_assign, map_assign, ":=", UTOperator::Assign);
make_enum_token!(UTOperator, op_add, map_add, "+", UTOperator::Add);
make_enum_token!(UTOperator, op_sub, map_sub, "-", UTOperator::Sub);
make_enum_token!(UTOperator, op_mult, map_mult, "*", UTOperator::Mult);
make_enum_token!(UTOperator, op_div, map_div, "/", UTOperator::Div);
make_enum_token!(UTOperator, op_equal, map_equal, "=", UTOperator::Equal);
make_enum_token!(UTOperator,
                 op_notequal,
                 map_notequal,
                 "!=",
                 UTOperator::NotEqual);
make_enum_token!(UTOperator, op_lt, map_lt, "<", UTOperator::LT);
make_enum_token!(UTOperator, op_gt, map_gt, ">", UTOperator::GT);
make_enum_token!(UTOperator,
                 op_openpar,
                 map_openpar,
                 "(",
                 UTOperator::OpenPar);
make_enum_token!(UTOperator,
                 op_closepar,
                 map_closepar,
                 ")",
                 UTOperator::ClosePar);
make_enum_token!(UTOperator, op_semi, map_semi, ";", UTOperator::Semi);
make_enum_token!(UTOperator, op_comma, map_comma, ",", UTOperator::Comma);
make_enum_token!(UTOperator, op_le, map_le, "<=", UTOperator::LE);
make_enum_token!(UTOperator, op_ge, map_ge, ">=", UTOperator::GE);

/// Select between operators
named!(pub scan_operator<UToken>,
       alt!(
           map!(op_assign, UToken::Operator) |
           map!(op_add, UToken::Operator) |
           map!(op_sub, UToken::Operator) |
           map!(op_mult, UToken::Operator) |
           map!(op_div, UToken::Operator) |
           map!(op_notequal, UToken::Operator) |
           map!(op_equal, UToken::Operator) |
           map!(op_le, UToken::Operator) |
           map!(op_ge, UToken::Operator) |
           map!(op_lt, UToken::Operator) |
           map!(op_gt, UToken::Operator) |
           map!(op_openpar, UToken::Operator) |
           map!(op_closepar, UToken::Operator) |
           map!(op_semi, UToken::Operator) |
           map!(op_comma, UToken::Operator)
           )
       );

#[cfg(test)]
mod tests {

    use scan::*;

    #[test]
    fn test_recognize_program() {
        let result = key_program("  PROGRAM".as_bytes());
        assert_eq!(result.to_result(), Ok(UTKeyword::Program));
    }

    #[test]
    fn test_recognize_not_if() {
        let result = key_if("  FI".as_bytes());
        assert!(result.to_result().is_err());
    }


    #[test]
    fn test_ignore_witespace_function() {
        let result = key_function("  \t   \r \nFUNCTION\t".as_bytes());
        assert_eq!(result.to_result(), Ok(UTKeyword::Function));
    }

    #[test]
    fn test_recognize_scan_begin() {
        let result = scan_keyword("BEGIN".as_bytes());
        assert_eq!(result.to_result(),
                   Ok(UToken::Keyword(UTKeyword::Begin)));
    }

    #[test]
    fn test_fail_scan_keyword_on_op() {
        let result = scan_keyword(" * ".as_bytes());
        assert!(result.to_result().is_err());
    }

    #[test]
    fn test_recognize_scan_return() {
        let result = scan_keyword("RETURN                ".as_bytes());
        assert_eq!(result.to_result(),
                   Ok(UToken::Keyword(UTKeyword::Return)));
    }

    #[test]
    fn test_recognize_scan_assign() {
        let result = scan_operator(":= ".as_bytes());
        assert_eq!(result.to_result(),
                   Ok(UToken::Operator(UTOperator::Assign)));
    }

    #[test]
    fn test_recognize_nested_add() {
        let result = op_add("+2".as_bytes());
        assert_eq!(result.to_result(), Ok(UTOperator::Add));
    }

    #[test]
    fn test_recognize_nested_op() {
        let result = scan_operator("+2".as_bytes());
        assert_eq!(result.to_result(),
                   Ok(UToken::Operator(UTOperator::Add)));
    }

    #[test]
    fn test_fail_scan_operator() {
        let result = scan_operator("   FUNCTION ".as_bytes());
        assert!(result.to_result().is_err());
    }

    #[test]
    fn test_recognize_comment_with_n() {
        let result = scan_comment("  \t -- foo bar \t baz\n foo".as_bytes());
        assert_eq!(result.to_result(), Ok(UToken::Comment));
    }

    #[test]
    fn test_recognize_comment_with_nr() {
        let result = scan_comment("  \n\t --foo bar \t baz\n\r foo".as_bytes());
        assert_eq!(result.to_result(), Ok(UToken::Comment));
    }

    #[test]
    fn test_fail_scan_non_comment() {
        let result = scan_comment("  \n\t foo bar \t baz\n\r foo".as_bytes());
        assert!(result.to_result().is_err());
    }

    #[test]
    fn test_recognize_string() {
        let result = scan_string("  \"foo bar\"  baz".as_bytes());
        assert_eq!(result.to_result(),
                   Ok(UToken::StringLiteral("\"foo bar\"".to_string())));
    }

    #[test]
    fn test_recognize_one_space_string() {
        let result = scan_string("  \" foo\"  baz".as_bytes());
        assert_eq!(result.to_result(),
                   Ok(UToken::StringLiteral("\" foo\"".to_string())));
    }

    #[test]
    fn test_recognize_float_no_sign_has_lead_follow() {
        let result = scan_float(" 1.01".as_bytes());
        assert_eq!(result.to_result(),
                   Ok(UToken::FloatLiteral("1.01".to_string())));
    }

    #[test]
    fn test_recognize_float_sign_has_follow() {
        let result = scan_float(" \t -.001".as_bytes());
        assert_eq!(result.to_result(),
                   Ok(UToken::FloatLiteral("-.001".to_string())));
    }

    #[test]
    fn test_recognize_float_sign_has_lead() {
        let result = scan_float(" \t -2345.0 ".as_bytes());
        assert_eq!(result.to_result(),
                   Ok(UToken::FloatLiteral("-2345.0".to_string())));
    }

    #[test]
    fn test_recognize_int_no_sign() {
        let result = scan_int(" \t 2345 ".as_bytes());
        assert_eq!(result.to_result(),
                   Ok(UToken::IntLiteral("2345".to_string())));
    }

    #[test]
    fn test_recognize_int_sign() {
        let result = scan_int(" \n\r -3145 ".as_bytes());
        assert_eq!(result.to_result(),
                   Ok(UToken::IntLiteral("-3145".to_string())));
    }

    #[test]
    fn test_recognize_id_no_num() {
        let result = scan_identifier("foobar n".as_bytes());
        assert_eq!(result.to_result(),
                   Ok(UToken::Identifier("foobar".to_string())));
    }

    #[test]
    fn test_recognize_id_with_num() {
        let result = scan_identifier("\t\n\r    foo2bar3 n".as_bytes());
        assert_eq!(result.to_result(),
                   Ok(UToken::Identifier("foo2bar3".to_string())));
    }

    #[test]
    fn test_fail_init_number() {
        let result = scan_identifier("   \t 2bar3 ".as_bytes());
        assert!(result.to_result().is_err());
    }

    #[test]
    fn test_get_token() {
        let result = get_token("  \t :=  foo bar".as_bytes());
        assert_eq!(result.to_result(),
                   Ok(UToken::Operator(UTOperator::Assign)));
    }

    #[test]
    fn test_tokenize() {
        let result = tokenize("  \t foo :=  bar * 3.14".as_bytes());
        assert_eq!(result.to_result(),
                   Ok(vec![UToken::Identifier("foo".to_string()),
                           UToken::Operator(UTOperator::Assign),
                           UToken::Identifier("bar".to_string()),
                           UToken::Operator(UTOperator::Mult),
                           UToken::FloatLiteral("3.14".to_string())]));
    }
}
