/// Data structures that describe the Micro grammar.
/// JWM

pub use tokens::*;

// program           -> PROGRAM id BEGIN pgm_body END
#[derive(Debug)]
pub struct Program {
    pub id: ID,
    pub p_body: PBody,
}

// id                -> IDENTIFIER
#[derive(Debug)]
pub struct ID {
    pub identifier: UToken,
}

// pgm_body          -> decl func_declarations
#[derive(Debug)]
pub struct PBody {
    pub decls: Option<Decl>,
    pub func_declarations: FuncDeclarations,
}

// decl                -> string_decl decl | var_decl decl | empty
// personal
// decl                -> some_decl decl | empty
#[derive(Debug)]
pub struct Decl {
    pub decl: Vec<SomeDecl>,
}

// personal
// some_decl           -> string_decl | var_decl
#[derive(Debug)]
pub enum SomeDecl {
    DStringDecl(StringDecl),
    DVarDecl(VarDecl),
}


// var_decl          -> var_type id_list ;
#[derive(Debug)]
pub struct VarDecl {
    pub var_type: VarType,
    pub id_list: IDList,
}

// string_decl    pub    -> STRING id := str ;
#[derive(Debug)]
pub struct StringDecl {
    pub id: ID,
    pub str_: Str_,
}


// str               -> STRINGLITERAL
#[derive(Debug)]
pub struct Str_ {
    pub string_literal: UToken,
}

// var_type            -> FLOAT | INT
#[derive(Debug)]
pub struct VarType {
    pub var: UTKeyword,
}

// any_type          -> var_type | VOID
#[derive(Debug)]
pub enum AnyType {
    AVarType(VarType),
    AVoid(UTKeyword),
}

// id_list           -> id id_tail
// personal:
// id_list           -> id, id, ...
#[derive(Debug)]
pub struct IDList {
    pub ids: Vec<ID>,
}

#[derive(Debug)]
pub struct PrimaryList {
    pub postfix_exprs: Vec<PostfixExpr>,
}

// param_decl_list   -> param_decl param_decl_tail | empty
// personal:
// id_list           -> param_decl, param_decl, ...
#[derive(Debug)]
pub struct ParamDeclList {
    pub param_decls: Vec<ParamDecl>,
}

// param_decl        -> var_type id
#[derive(Debug)]
pub struct ParamDecl {
    pub var_type: VarType,
    pub id: ID,
}

// func_declarations -> func_decl func_declarations | empty
// personal:
// func_declarations -> func_decl func_decl ...
#[derive(Debug)]
pub struct FuncDeclarations {
    pub func_declarations: Vec<FuncDecl>,
}

// func_decl         -> FUNCTION any_type id (param_decl_list) BEGIN func_body END
#[derive(Debug)]
pub struct FuncDecl {
    pub any_type: AnyType,
    pub id: ID,
    pub param_decl_list: Option<ParamDeclList>,
    pub func_body: FuncBody,
}

// func_body         -> decl stmt_list
#[derive(Debug)]
pub struct FuncBody {
    pub decl: Option<Decl>,
    pub stmt_list: StmtList,
}

// stmt_list         -> stmt stmt_list | empty
// personal:
// stmt_list         -> stmt, stmt, ...
#[derive(Debug)]
pub struct StmtList {
    pub stmts: Vec<Stmt>,
}

// stmt              -> base_stmt | if_stmt | for_stmt
#[derive(Debug)]
pub enum Stmt {
    SBaseStmt(BaseStmt),
    SIfStmt(IfStmt),
    SForStmt(ForStmt),
}

// base_stmt         -> assign_stmt | read_stmt | write_stmt | return_stmt
#[derive(Debug)]
pub enum BaseStmt {
    BAssignStmt(AssignStmt),
    BReadStmt(ReadStmt),
    BWriteStmt(WriteStmt),
    BReturnStmt(ReturnStmt),
}

// assign_stmt    pub    -> assign_expr ;
#[derive(Debug)]
pub struct AssignStmt {
    pub assign_expr: AssignExpr,
}

// assign_expr    pub    -> id := expr
#[derive(Debug)]
pub struct AssignExpr {
    pub id: ID,
    pub expr: Expr,
}

// read_stmt         -> READ ( id_list );
#[derive(Debug)]
pub struct ReadStmt {
    pub expr_list: ExprList,
}

// write_stmt        -> WRITE ( id_list );
#[derive(Debug)]
pub struct WriteStmt {
    pub expr_list: ExprList,
}

// return_stmt    pub    -> RETURN expr ;
#[derive(Debug)]
pub struct ReturnStmt {
    pub expr: Expr,
}

// expr              -> expr_prefix factor
// personal
// expr              -> factor addop factor ... addop factor
#[derive(Debug)]
pub struct Expr {
    pub expr_prefixes: Vec<ExprPrefix>,
    pub factor: Factor,
}

// expr_prefix    pub    -> expr_prefix factor addop | empty
#[derive(Debug)]
pub struct ExprPrefix {
    pub factor: Factor,
    pub addop: Addop,
}

// factor            -> factor_prefix postfix_expr
#[derive(Debug)]
pub struct Factor {
    pub factor_prefixes: Vec<FactorPrefix>,
    pub postfix_expr: Box<PostfixExpr>,
}

// factor_prefix    pub  -> factor_prefix postfix_expr mulop | empty
#[derive(Debug)]
pub struct FactorPrefix {
    pub postfix_expr: PostfixExpr,
    pub mulop: Mulop,
}

// postfix_expr    pub   -> primary | call_expr
#[derive(Debug)]
pub enum PostfixExpr {
    PPrimary(Primary),
    PCallExpr(CallExpr),
}

// call_expr         -> id ( expr_list )
#[derive(Debug)]
pub struct CallExpr {
    pub id: ID,
    pub expr_list: Option<ExprList>,
}

// expr_list         -> expr expr_list_tail | empty
#[derive(Debug)]
pub struct ExprList {
    pub exprs: Vec<Expr>,
}

// primary           -> ( expr ) | id | INTLITERAL | FLOATLITERAL
#[derive(Debug)]
pub enum Primary {
    PExpr(Expr),
    PID(ID),
    PIntLiteral(UToken),
    PFloatLiteral(UToken),
}

// addop             -> + | -
#[derive(Debug)]
pub struct Addop {
    pub op: UTOperator,
}

// mulop             -> * | /
#[derive(Debug)]
pub struct Mulop {
    pub op: UTOperator,
}

// if_stmt           -> IF ( cond ) decl stmt_list else_part FI
#[derive(Debug)]
pub struct IfStmt {
    pub cond: Cond,
    pub decl: Option<Decl>,
    pub stmt_list: StmtList,
    pub else_part: Option<ElsePart>,
    pub scope_name: String,
}

// else_part         -> ELSE decl stmt_list | empty
#[derive(Debug)]
pub struct ElsePart {
    pub decl: Decl,
    pub stmt_list: Option<StmtList>,
    pub scope_name: String,
}

// cond              -> expr compop expr
#[derive(Debug)]
pub struct Cond {
    pub expr1: Expr,
    pub compop: Compop,
    pub expr2: Expr,
}

// compop            -> < | > | = | != | <= | >=
#[derive(Debug)]
pub struct Compop {
    pub op: UTOperator,
}

// init_stmt         -> assign_expr | empty
#[derive(Debug)]
pub struct InitStmt {
    pub assign_expr: Option<AssignExpr>,
}

// incr_stmt         -> assign_expr | empty
#[derive(Debug)]
pub struct IncrStmt {
    pub assign_expr: Option<AssignExpr>,
}

// for_stmt          -> FOR ( init_stmt ; cond ; incr_stmt ) decl stmt_list ROF
#[derive(Debug)]
pub struct ForStmt {
    pub init_stmt: InitStmt,
    pub cond: Cond,
    pub incr_stmt: IncrStmt,
    pub decl: Option<Decl>,
    pub stmt_list: StmtList,
    pub scope_name: String,
}
