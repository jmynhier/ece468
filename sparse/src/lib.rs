/// Scanner/Parser interface for Micro compiler written for Purdue ECE 46800 Fall 2017.
/// JWM
#[macro_use]
extern crate nom;

pub mod tokens;
pub mod scan;
pub mod blocks;
pub mod parse;
pub mod symbols;

// Reexport the structs in tokens for interface use.
pub use tokens::*;
pub use blocks::*;
pub use symbols::*;
pub use parse::parse_program;
