/// Data structures used to create the symbol table for step 3.
/// JWM

use std::fmt::{self, Display, Debug};
use std::cmp::PartialEq;
use std::collections::HashMap;
use tokens::UTKeyword;

/// Holds one declaration of any type
#[derive(Clone)]
pub struct Symbol {
    pub var_name: String,
    pub type_name: UTKeyword, // keyword
    pub string_value: Option<String>, // optional string literal
}

impl PartialEq for Symbol {
    fn eq(&self, other: &Symbol) -> bool {
        self.var_name == other.var_name
    }
}

// For step 3
impl Display for Symbol {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let result = format!("type {}", self.type_name);
        if let Some(ref sv) = self.string_value {
            write!(f, "{} value {}", result, sv)
        } else {
            write!(f, "{}", result)
        }
    }
}

impl Debug for Symbol {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self)
    }
}

/// Holds a local symbol table.
/// Meant to be held in a HashMap<String, SymbolTable>, where the
/// String is the name of the scope as held in the parse tree and AST.
/// - parent: The name of the parent scope. Only Global has None.
#[derive(Clone)]
pub struct SymbolTable {
    pub name: String,
    pub return_type: Option<UTKeyword>,
    pub symbols: HashMap<String, Symbol>,
    pub params: HashMap<String, Symbol>,
    pub param_order: Vec<String>,
    pub parent: Option<String>,
    pub children: Vec<String>,
}

impl Display for SymbolTable {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut result = String::default();

        result += &format!("{} returns: {:?}, parent: {}",
                           self.name, self.return_type, 
                           match self.parent {
                               Some(ref p) => p,
                               _ => "None",
                           });
        result += &format!(", Children :\n");
        for child in &self.children {
            result += &format!("{}", child);
        }

        result += &format!("Parameters: ");

        for (name, symbol) in &self.params {
            result += &format!("\n{}: {}", name, symbol.to_string());
        }
        result += &format!("\nSymbol table: ");

        for (name, symbol) in &self.symbols {
            result += &format!("\n{}: {}", name, symbol.to_string());
        }

        write!(f, "{}", result)
    }
}

impl Debug for SymbolTable {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "\n{}\n", self)
    }
}

/// Symbol Table Error state.
/// Currently only represents a declaration error.
/// (multiple same ids in same scope).
#[derive(Debug)]
pub struct STErr {
    pub name: String,
}

impl Display for STErr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "DECLARATION ERROR {}", self.name)
    }
}
