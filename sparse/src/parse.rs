/// Macros that parse input tokens.
/// JWM

pub use blocks::*;
pub use symbols::*;
use std::collections::HashMap;
use scan::*;
use std::boxed::Box;
use nom::line_ending;

static mut BLOCK_COUNT: i32 = 1;

type SymbolMap = HashMap<String, SymbolTable>;
type SymbolRes = Result<SymbolMap, STErr>;

/// Parses an entire program. Entry point for parser.
named!(pub parse_program<(Program, SymbolRes)>,
       ws!(do_parse!(
               opt!(parse_comments) >>
               ws!(tag!("PROGRAM")) >>
               opt!(parse_comments) >>
               id: parse_id >>
               opt!(parse_comments) >>
               ws!(tag!("BEGIN")) >>
               opt!(parse_comments) >>
               p_body_and_table: parse_p_body >>
               opt!(parse_comments) >>
               ws!(tag!("END")) >>
               opt!(complete!(parse_comments)) >>

               ({
                   let (p_body, symbol_table) = p_body_and_table;
                   (Program { id: id, p_body: p_body }, symbol_table)
               })
               )
           )
        );

/// Parses identifier tokens.
named!(parse_id<ID>,
       ws!(do_parse!(
               id: scan_identifier >>

               (ID { identifier: id })
               )
           )
        );

/// Helper that unpacks Option<(Block, HashMap<Symbol>)> to
/// (Option<Box>, Vec<Symbol>).
fn split_symbols<U, T>(input: Option<(U, T)>) -> (Option<U>, T)
    where T: Default
{
    if input.is_some() {
        let raw = input.unwrap();
        (Some(raw.0), raw.1)
    } else {
        (None, T::default())
    }
}

/// Helper that checks for duplicate identifiers in same scope
/// in a list of Symbols.
fn has_symbol_dupes(symbols: &Vec<Symbol>) -> Option<STErr> {
    let mut consumable = symbols
        .iter()
        .map(|symbol| symbol.clone())
        .rev()
        .collect::<Vec<_>>();
    while let Some(symbol) = consumable.pop() {
        if consumable.contains(&symbol) {
            return Some(STErr { name: symbol.var_name });
        }
    }
    None
}

/// Helper that constructs the local SymbolTable.
/// It scans a the list of sub tables. If it finds an
/// error, it returns it, otherwise it returns the ST.
fn symbol_table_new(name: String,
                    return_type: Option<UTKeyword>,
                    symbols: Vec<Symbol>,
                    mut sub_tables: Vec<SymbolRes>,
                    params: Vec<Symbol>)
                    -> SymbolRes {
    // Evaluate symbols.
    let err = has_symbol_dupes(&symbols);

    // Evaluate subtables.
    let mut sub_err: Option<Result<_, STErr>> = None;
    let mut err_index = sub_tables.len() + 1;
    for (index, ref table) in sub_tables.iter().enumerate() {
        if table.is_err() {
            err_index = index;
            break;
        }
    }
    // Pop the first found error.
    if err_index != sub_tables.len() + 1 {
        sub_err = Some(sub_tables.remove(err_index));
    }

    // Construct the result.
    if err.is_none() && sub_err.is_none() {
        // No errors.
        let mut symbol_map = HashMap::new();
        for symbol in symbols {
            symbol_map.insert(symbol.var_name.clone(), symbol);
        }

        // Construct the parameter object.
        let mut param_map = HashMap::new();
        let mut param_order = Vec::new();
        for symbol in params {
            param_order.push(symbol.var_name.clone());
            param_map.insert(symbol.var_name.clone(), symbol);
        }

        // Get your childrens' names.
        let children = sub_tables
            .iter()
            .flat_map(|res| get_names(res))
            .filter(|name| name != "")
            .collect::<Vec<String>>();

        let symbol_table = SymbolTable {
            name: name.clone(),
            return_type: return_type,
            symbols: symbol_map,
            children: children,
            parent: None,
            params: param_map,
            param_order: param_order,
        };

        // Add this table and its children to the global table.
        let mut table_map = HashMap::new();
        table_map.insert(name.clone(), symbol_table);
        for res in sub_tables.into_iter() {
            if let Ok(mut sub_table) = res {
                for mut table in sub_table.values_mut() {
                    if table.parent.is_none() {
                        table.parent = Some(name.clone());
                    }
                }
                table_map.extend(sub_table);
            }
        }

        Ok(table_map)
    } else if err.is_some() {
        // Local symbol error.
        Err(err.unwrap())
    } else {
        // Error in a sub table.
        sub_err.unwrap()
    }
}

fn get_names(table: &SymbolRes) -> Vec<String> {
    match table {
        &Ok(ref item) => {
            item.keys()
                .map(|key| key.clone())
                .collect::<Vec<String>>()
        }
        _ => Vec::new(),
    }
}

/// Parses program body.
/// p_body -> decl func_declarations
named!(parse_p_body<(PBody, SymbolRes)>,
       ws!(do_parse!(
               opt!(parse_comments) >>
               decls_and_symbol_table: opt!(parse_decl) >>
               opt!(parse_comments) >>
               func_decls_and_symbol_tables: parse_func_declarations >>
               opt!(complete!(parse_comments)) >>

                   ({
                       // Get local symbols and check for errors.
                       let (decls, symbols) = split_symbols(decls_and_symbol_table);

                       // Get sub tables and check for errors.
                       let (func_decls, func_symbol_tables) = func_decls_and_symbol_tables;

                       let symbol_table = symbol_table_new("GLOBAL".to_string(), 
                                                           None,
                                                           symbols,
                                                           func_symbol_tables,
                                                           Vec::new() /* no params*/);

                       (PBody { 
                           decls: decls, 
                           func_declarations: func_decls,
                            },
                        symbol_table
                       )
                   })
               )
           )
        );

/// Helper function that unpacks a token string.
fn token_unpack(input: UToken) -> String {
    match input {
        UToken::Identifier(x) => x,
        UToken::IntLiteral(x) => x,
        UToken::FloatLiteral(x) => x,
        UToken::StringLiteral(x) => x,
        _ => "ERR".to_string(),
    }
}

/// Parses an optional declaration.
/// decl -> some_decl | empty
named!(parse_decl<(Decl, Vec<Symbol>)>,
       ws!(do_parse!(
               some_decl: many0!(do_parse!(
                       opt!(parse_comments) >>
                       element: parse_some_decl >>
                       opt!(complete!(parse_comments)) >>

                       (element)
                       )) >>
           
           ({   
               // Generate single level list of symbols.
               let mut symbols: Vec<Symbol> = Vec::new();
               for ref decl in some_decl.iter() {
                   match decl {
                       &&SomeDecl::DStringDecl(ref string_decl) => {
                           let var_name = token_unpack(string_decl.id.identifier.clone());
                           let literal = Some(token_unpack(string_decl.str_.string_literal.clone()));
                           symbols.push(Symbol {
                               var_name: var_name,
                               type_name: UTKeyword::String_,
                               string_value: literal,
                           })
                       },
                       &&SomeDecl::DVarDecl(ref var_decl) => {
                           for id in var_decl.id_list.ids.iter() {

                               let var_name = token_unpack(id.identifier.clone());
                                   
                               symbols.push(Symbol {
                                   var_name: var_name,
                                   type_name: var_decl
                                       .var_type
                                       .var
                                       .clone(),
                                   string_value: None,
                               })
                           }
                       },
                   }
               };

               (Decl { decl: some_decl }, symbols)
           })
           )
           )
       );

/// Parses string or var declarations
/// some_decl -> string_decl | var_decl
named!(parse_some_decl<SomeDecl>,
       ws!(alt!(
               parse_string_decl |
               parse_var_decl
               )
           )
        );

/// Parses a variable declaration.
/// var_decl -> var_type id_list ;
named!(parse_var_decl<SomeDecl>,
       ws!(do_parse!(
               opt!(parse_comments) >>
               var_type: parse_var_type >>
               id_list: parse_id_list >>
               tag!(";") >>
               opt!(complete!(parse_comments)) >>

               (SomeDecl::DVarDecl(VarDecl {
                   var_type: var_type,
                   id_list: id_list,
               }))
               )
           )
       );

/// Parses a string literal declaration.
/// string_decl -> STRING id := str ;
named!(parse_string_decl<SomeDecl>,
       ws!(do_parse!(
               opt!(parse_comments) >>
               ws!(tag!("STRING")) >>
               id: parse_id >>
               tag!(":=") >>
               str_: parse_str_ >>
               tag!(";") >>
               opt!(complete!(parse_comments)) >>

               (SomeDecl::DStringDecl(StringDecl{
                   id: id,
                   str_: str_,
               }))
               )
           )
       );

/// Parses a string literal.
/// str_ -> STRINGLITERAL
named!(parse_str_<Str_>,
       ws!(do_parse!(
               str_: scan_string >>

               (Str_ { string_literal: str_ })
               )
           )
       );

/// Parses a variable
/// var_type => FLOAT | INT
named!(parse_var_type<VarType>,
       ws!(do_parse!(
               var: ws!(alt_complete!(key_float | key_int)) >>

               (VarType { var: var })
               )
           )
       );

/// Parses a variable of any type.
/// any_type -> var_type | VOID
named!(parse_any_type<AnyType>,
       ws!(alt_complete!(
               do_parse!(
                   var_type: parse_var_type >>

                   (AnyType::AVarType(var_type))
                   ) |
               do_parse!(
                   void_key: key_void >>

                   (AnyType::AVoid(void_key))
                   )
               )
           )
        );

/// Parses a list of identifiers.
/// id_list -> id, id, ...
named!(parse_id_list<IDList>,
       ws!(do_parse!(
               ids: separated_nonempty_list_complete!(
                   ws!(tag!(",")), 
                   parse_id
                   ) >>

               (IDList { ids: ids })
               )
           )
       );

/// Parses a list of parameter declarations
/// param_decl_list -> param_decl, param_decl, ... | empty
named!(parse_param_decl_list<(ParamDeclList, Vec<Symbol>)>,
       ws!(do_parse!(
               param_decls: separated_list_complete!(
                   ws!(tag!(",")), 
                   parse_param_decl
                   ) >>

               ({
                   let symbols = param_decls
                       .iter()
                       .map(|param_decl| {
                           Symbol {
                               var_name: token_unpack(param_decl.id.identifier.clone()),
                               type_name: param_decl.var_type.var.clone(),
                               string_value: None,
                           }
                       })
                       .collect::<Vec<_>>();

                   (ParamDeclList { param_decls: param_decls },
                    symbols)
               })
               )
           )
       );

/// Parses a single parameter declaration.
/// param_decl -> var_type id
named!(parse_param_decl<ParamDecl>,
       ws!(do_parse!(
               var_type: parse_var_type >>
               id: parse_id >>

               (ParamDecl { var_type: var_type, id: id })
               )
           )
       );

/// Parses a list of function declarations
/// func_decl -> func_decl func_decl ...
named!(parse_func_declarations<(FuncDeclarations, Vec<SymbolRes>)>,
       ws!(do_parse!(
               opt!(parse_comments) >>
               func_decls_and_symbol_tables: many0!(parse_func_decl) >>
               opt!(complete!(parse_comments)) >>

               ({
                   let mut func_decls = Vec::new();
                   let mut symbol_tables = Vec::new();
                   for item in func_decls_and_symbol_tables {
                       func_decls.push(item.0);
                       symbol_tables.push(item.1);
                   }
                   (FuncDeclarations { func_declarations: func_decls },
                    symbol_tables)
               })
               )
           )
       );

/// Parses a single function declaration.
/// func_decl -> FUNCTION any_type id (param_decl_list) BEGIN func_body END
named!(parse_func_decl<(FuncDecl, SymbolRes)>,
       ws!(do_parse!(
               opt!(parse_comments) >>
               ws!(tag!("FUNCTION")) >>
               any_type: parse_any_type >>
               id: parse_id >>
               ws!(tag!("(")) >>
               params_and_symbols: opt!(parse_param_decl_list) >>
               ws!(tag!(")")) >>
               opt!(parse_comments) >>
               opt!(line_ending) >>
               opt!(parse_comments) >>
               ws!(tag!("BEGIN")) >>
               opt!(parse_comments) >>
               func_body_and_symbols: ws!(parse_func_body) >>
               opt!(parse_comments) >>
               ws!(tag!("END")) >> 
               opt!(complete!(parse_comments)) >>
               
               ({
                   let (params, param_symbols) = split_symbols(params_and_symbols);
                   
                   let (func_body, body_symbols, sub_tables) = func_body_and_symbols;
                   

                   let return_type = match &any_type {
                       &AnyType::AVarType(ref var) => var.var.clone(),
                       _ => UTKeyword::Void,
                   };

                   let symbol_table = symbol_table_new(token_unpack(id.identifier.clone()), 
                                                       Some(return_type),
                                                       body_symbols, 
                                                       sub_tables,
                                                       param_symbols);

                   let func_decl = FuncDecl {
                       any_type: any_type,
                       id: id,
                       param_decl_list: params,
                       func_body: func_body,
                   };

                   (func_decl, symbol_table)
               })
               )
           )
       );

/// Parses a function body.
/// func_body -> decl stmt_list
named!(parse_func_body<(FuncBody, Vec<Symbol>, Vec<SymbolRes>)>,
       ws!(do_parse!(
               opt!(parse_comments) >>
               decl_and_symbols: opt!(parse_decl) >>
               opt!(parse_comments) >>
               stmt_list_and_symbol_tables: parse_stmt_list >>
               opt!(complete!(parse_comments)) >>
               
               ({
                   let (decl, symbols) = split_symbols(decl_and_symbols);

                   let (stmt_list, symbol_tables) = stmt_list_and_symbol_tables;

                   (FuncBody {decl: decl, stmt_list: stmt_list },
                    symbols, symbol_tables)
               })
               )
           )
       );

/// Parses a list of one or more statements
/// stmt_list -> stmt stmt ...
named!(parse_stmt_list<(StmtList, Vec<SymbolRes>)>,
       ws!(do_parse!(
               opt!(line_ending) >>
               stmts_and_symbol_tables: many0!(parse_stmt) >>
               
               ({
                   let mut stmts = Vec::new();
                   let mut symbol_tables = Vec::new();
                   for (stmt, symbol_table) in stmts_and_symbol_tables {
                       stmts.push(stmt);
                       if let Some(st) = symbol_table {
                           symbol_tables.push(st);
                       }
                   }
                   (StmtList {stmts: stmts}, symbol_tables)
               })
               )
           )
       );

/// Parses a statement
/// stmt -> base_stmt | if_stmt | for_stmt
named!(parse_stmt<(Stmt, Option<SymbolRes>)>,
       ws!(alt_complete!(
               parse_base_stmt |
               parse_if_stmt |
               parse_for_stmt
               )
           )
        );

/// Parses a base statement
/// base_stmt -> assign_stmt | read_stmt | write_stmt | return_stmt
named!(parse_base_stmt<(Stmt, Option<SymbolRes>)>,
       ws!(do_parse!(
               stmt: alt_complete!(
                   parse_read_stmt |
                   parse_write_stmt |
                   parse_assign_stmt |
                   parse_return_stmt
                   ) >>

               (stmt, None)
               )
           )
        );

/// Parses an assignment statment
/// assign_stmt -> assign_expr;
named!(parse_assign_stmt<Stmt>,
       ws!(do_parse!(
               opt!(parse_comments) >>
               assign_expr: parse_assign_expr >>
               tag!(";") >>
               opt!(complete!(parse_comments)) >>

               (Stmt::SBaseStmt(BaseStmt::BAssignStmt(AssignStmt {
                   assign_expr: assign_expr
               })))
               )
           )
        );

/// Parses an assign expression
/// assign_expr -> id := expr ;
named!(parse_assign_expr<AssignExpr>,
       ws!(do_parse!(
               opt!(parse_comments) >>
               id: ws!(parse_id) >>
               tag!(":=") >>
               expr: parse_expr >>
               opt!(complete!(parse_comments)) >>

               (AssignExpr { id: id, expr: expr })
               )
           )
        );

/// Parses a read statement
/// read_stmt -> READ ( id_list );
named!(parse_read_stmt<Stmt>,
       ws!(do_parse!(
               opt!(parse_comments) >>
               ws!(tag!("READ")) >>
               tag!("(") >>
               id_list: parse_expr_list >>
               tag!(")") >>
               tag!(";") >>
               opt!(complete!(parse_comments)) >>

               (Stmt::SBaseStmt(BaseStmt::BReadStmt(ReadStmt {
                   expr_list: id_list,
               })))
               )
           )
        );

/// Parses a write statement
/// write_stmt -> WRITE ( id_list );
named!(parse_write_stmt<Stmt>,
       ws!(do_parse!(
               opt!(parse_comments) >>
               ws!(tag!("WRITE")) >>
               tag!("(") >>
               id_list: parse_expr_list >>
               tag!(")") >>
               tag!(";") >>
               opt!(complete!(parse_comments)) >>

               (Stmt::SBaseStmt(BaseStmt::BWriteStmt(WriteStmt {
                   expr_list: id_list,
               })))
               )
           )
        );

/// Parses a return statement
/// return_stmt -> RETURN expr ;
named!(parse_return_stmt<Stmt>,
       ws!(do_parse!(
               opt!(parse_comments) >>
               ws!(tag!("RETURN")) >>
               expr: parse_expr >>
               tag!(";") >>
               opt!(complete!(parse_comments)) >>

               (Stmt::SBaseStmt(BaseStmt::BReturnStmt(ReturnStmt {
                   expr: expr,
               })))
               )
           )
        );

/// Parses a single expression
/// expr -> factor addop factor ... addop factor
named!(parse_expr<Expr>,
       ws!(do_parse!(
               expr_prefixes: many0!(parse_expr_prefix) >>
               factor: parse_factor >>

               (Expr { 
                   expr_prefixes: expr_prefixes,
                   factor: factor, 
               })
               )
           )
        );

/// Parses an expression prefix
/// expr_prefix -> factor addop
named!(parse_expr_prefix<ExprPrefix>,
       ws!(do_parse!(
               factor: parse_factor >>
               addop: parse_addop >>

               (ExprPrefix { 
                   factor: factor,
                   addop: addop,
               })
               )
           )
        );

/// Parses a factor.
/// factor -> postfix_expr mulop postfix_expr ... mulop postfix_expr
named!(parse_factor<Factor>,
       ws!(do_parse!(
               factor_prefixes: many0!(parse_factor_prefix) >>
               postfix_expr: parse_postfix_expr >>

               ({
                   let prefixes = factor_prefixes.into_iter().collect::<Vec<_>>();
                   Factor { 
                       factor_prefixes: prefixes,
                       postfix_expr: Box::new(postfix_expr),
                   }
               }))
           )
        );

/// Parses a factor prefix.
/// factor_prefix -> postfix_expr mulop
named!(parse_factor_prefix<FactorPrefix>,
       ws!(do_parse!(
               postfix_expr: parse_postfix_expr >>
               mulop: parse_mulop >>

               (FactorPrefix { 
                   postfix_expr: postfix_expr,
                   mulop: mulop,
               })
               )
           )
        );

/// Parses a postfix expression
/// postfix_expr -> primary | call_expr
named!(parse_postfix_expr<PostfixExpr>,
       ws!(alt_complete!(
               parse_call_expr |
               parse_primary
               )
           )
        );

/// Parses a call expression.
/// call_expr -> id ( expr_list )
named!(parse_call_expr<PostfixExpr>,
       ws!(do_parse!(
               id: ws!(parse_id) >>
               ws!(tag!("(")) >>
               expr_list: opt!(ws!(parse_expr_list)) >>
               ws!(tag!(")")) >>

               (PostfixExpr::PCallExpr(CallExpr {
                   id: id,
                   expr_list: expr_list,
               }))
               )
           )
        );

/// Parses a list of expressions.
/// expr_list -> expr expr ...
named!(parse_expr_list<ExprList>,
       ws!(do_parse!(
               exprs: separated_nonempty_list_complete!(
                   ws!(tag!(",")), 
                   parse_expr
                   ) >>

               (ExprList { exprs: exprs })
               )
           )
        );

/// Parses a primary expression
/// primary -> ( expr ) | id | INTLITERAL | FLOATLITERAL
named!(parse_primary<PostfixExpr>,
       ws!(alt_complete!(
               parse_primary_expr |
               parse_primary_id |
               parse_primary_float_literal |
               parse_primary_int_literal
               )
           )
        );

/// Parses an expression for a primary expression.
named!(parse_primary_expr<PostfixExpr>,
       ws!(do_parse!(
               tag!("(") >>
               expr: parse_expr >>
               tag!(")") >>

               (PostfixExpr::PPrimary(Primary::PExpr(expr)))
               )
           )
        );

/// Parses an identifier for a primary expression.
named!(parse_primary_id<PostfixExpr>,
       ws!(do_parse!(
               id: parse_id>>

               (PostfixExpr::PPrimary(Primary::PID(id)))
               )
           )
        );

/// Parses an integer for a primary expression.
named!(parse_primary_int_literal<PostfixExpr>,
       ws!(do_parse!(
               token: scan_int >>

               (PostfixExpr::PPrimary(Primary::PIntLiteral(token)))
               )
           )
        );

/// Parses a float for a primary expression.
named!(parse_primary_float_literal<PostfixExpr>,
       ws!(do_parse!(
               token: scan_float >>

               (PostfixExpr::PPrimary(Primary::PFloatLiteral(token)))
               )
           )
        );

/// Parses a + or - operator
/// addop -> + | -
named!(parse_addop<Addop>,
       ws!(do_parse!(
               token: alt_complete!(op_add | op_sub) >>

               (Addop { op: token })
               )
           )
        );

/// Parses a * or / operator
/// mulop -> * | /
named!(parse_mulop<Mulop>,
       ws!(do_parse!(
               token: alt_complete!(ws!(op_mult) | ws!(op_div)) >>

               (Mulop { op: token })
               )
           )
        );

/// Parses an if statment
/// if_stmt -> IF ( cond ) decl stmt_list else_part FI
named!(parse_if_stmt<(Stmt, Option<SymbolRes>)>,
       ws!(do_parse!(
               opt!(parse_comments) >>
               ws!(tag!("IF")) >>
               tag!("(") >>
               cond: parse_cond >>
               tag!(")") >>
               opt!(parse_comments) >>
               decl_and_symbols: opt!(parse_decl) >>
               opt!(parse_comments) >>
               stmt_list_and_sub_tables: parse_stmt_list >>
               opt!(parse_comments) >>
               else_part_and_symbol_table: opt!(parse_else_part) >>
               opt!(parse_comments) >>
               ws!(tag!("FI")) >>
               opt!(complete!(parse_comments)) >>

               ({
                   let (decl, symbols) = split_symbols(decl_and_symbols);

                   let (stmt_list, mut sub_tables) = stmt_list_and_sub_tables;

                   let else_part = if else_part_and_symbol_table.is_some() {
                       let raw = else_part_and_symbol_table.unwrap();
                       sub_tables.push(raw.1);
                       Some(raw.0)
                   } else {
                       None
                   };

                   let name = unsafe {
                       BLOCK_COUNT += 1;
                       format!("IF_BLOCK{}", BLOCK_COUNT - 1)
                   };

                   let symbol_table = symbol_table_new(name.clone(),
                                                       None,
                                                       symbols,
                                                       sub_tables,
                                                       Vec::new());
                  
                   let if_stmt = Stmt::SIfStmt(IfStmt {
                       cond: cond,
                       decl: decl,
                       stmt_list: stmt_list,
                       else_part: else_part,
                       scope_name: name,
                   });

                   (if_stmt, Some(symbol_table))
               })
               )
           )
        );

/// Parses multiple lines of comments.
named!(parse_comments<()>,
    ws!(do_parse!(
            many1!(alt_complete!(scan_comment)) >>
            many0!(line_ending) >>

            (())
            )
        )
    );

/// Parses the else part of an if statment
/// else_part -> ELSE decl stmt_list
named!(parse_else_part<(ElsePart, SymbolRes)>,
       ws!(do_parse!(
               opt!(parse_comments) >>
               ws!(tag!("ELSE")) >>
               opt!(parse_comments) >>
               decl_and_symbols: parse_decl >>
               opt!(parse_comments) >>
               stmt_list_and_sub_tables: opt!(parse_stmt_list) >>
               opt!(complete!(parse_comments)) >>

               ({
                   let (decl, symbols) = decl_and_symbols;
                   let (stmt_list, sub_tables) = split_symbols(stmt_list_and_sub_tables);

                   let name = unsafe {
                       BLOCK_COUNT += 1;
                       format!("ELSE_BLOCK{}", BLOCK_COUNT - 1)
                   };
                   let symbol_table = symbol_table_new(name.clone(),
                                                       None,
                                                       symbols,
                                                       sub_tables,
                                                       Vec::new());

                   let else_part = ElsePart {
                       decl: decl,
                       stmt_list: stmt_list,
                       scope_name: name,
                   };

                   (else_part, symbol_table)
               })
               )
           )
        );

/// Parses a condition.
/// cond -> expr compop expr
named!(parse_cond<Cond>,
       ws!(do_parse!(
               expr1: parse_expr >>
               compop: parse_compop >>
               expr2: parse_expr >>

               (Cond {
                   expr1: expr1,
                   compop: compop,
                   expr2: expr2,
               })
               )
           )
        );

/// Parses a conditional operator.
/// compop -> < | > | = | != | <= | >=
named!(parse_compop<Compop>,
       ws!(do_parse!(
               op: alt_complete!(
                   op_le |
                   op_ge |
                   op_notequal |
                   op_equal |
                   op_lt |
                   op_gt
                   ) >>

               (Compop { op: op })
               )
           )
        );

/// Parses an initialization statement.
/// init_stmt -> assign_expr | empty
named!(parse_init_stmt<InitStmt>,
       ws!(do_parse!(
               assign_expr: opt!(parse_assign_expr) >>

               (InitStmt { assign_expr: assign_expr })
               )
           )
        );

/// Parses an increment statement.
/// incr_stmt -> assign_expr | empty
named!(parse_incr_stmt<IncrStmt>,
       ws!(do_parse!(
               assign_expr: opt!(parse_assign_expr) >>

               (IncrStmt { assign_expr: assign_expr })
               )
           )
        );

/// Parses a for statment.
/// for_stmt -> FOR ( init_stmt ; cond ; incr_stmt ) decl stmt_list ROF
named!(parse_for_stmt<(Stmt, Option<SymbolRes>)>,
       ws!(do_parse!(
               opt!(parse_comments) >>
               ws!(tag!("FOR")) >>
               tag!("(") >>
               init_stmt: parse_init_stmt >>
               tag!(";") >>
               cond: parse_cond >>
               tag!(";") >>
               incr_stmt: parse_incr_stmt >>
               tag!(")") >>
               opt!(parse_comments) >>
               decl_and_symbols: opt!(parse_decl) >>
               opt!(parse_comments) >>
               stmt_list_and_table: parse_stmt_list >>
               opt!(parse_comments) >>
               ws!(tag!("ROF")) >>
               opt!(complete!(parse_comments)) >>

               ({
                   let (decl, symbols) = split_symbols(decl_and_symbols);
                   
                   let (stmt_list, sub_tables) = stmt_list_and_table;

                   let name = unsafe {
                       BLOCK_COUNT += 1;
                       format!("FOR_BLOCK{}", BLOCK_COUNT - 1)
                   };

                   let symbol_table = symbol_table_new(name.clone(),
                                                       None,
                                                       symbols,
                                                       sub_tables,
                                                       Vec::new());

                   (Stmt::SForStmt(ForStmt { 
                       init_stmt: init_stmt,
                       cond: cond,
                       incr_stmt: incr_stmt,
                       decl: decl,
                       stmt_list: stmt_list,
                       scope_name: name,
                   }), Some(symbol_table))
               })
               )
           )
        );

#[cfg(test)]
mod tests {

    use parse::*;

    #[test]
    fn test_func() {
        let result = parse_func_decl("FUNCTION INT main(INT x)\nBEGIN\nRETURN 5;\nEND\n"
                                         .as_bytes());
        assert!(result.to_result().is_ok())
    }

    #[test]
    fn test_func_to_params() {
        let result = parse_func_decl("FUNCTION INT main()\nBEGIN\nINT x;\nx:=5;\nRETURN x;\nEND\n"
                                         .as_bytes());
        assert!(result.to_result().is_ok())
    }

    #[test]
    fn test_empty_program() {
        let result = parse_program("PROGRAM foo\nBEGIN\nEND\n".as_bytes());
        assert!(result.to_result().is_ok())
    }

    #[test]
    fn test_declarations() {
        let result = parse_decl("INT x;\n--foobar\nFLOAT z;\nINT y;".as_bytes());
        assert!(result.to_result().is_ok())
    }

    #[test]
    fn test_long_func() {
        let result = parse_func_decl("\n\t\n\tFUNCTION INT main()\n\tBEGIN\n\n\tc:=a+b;\n\tb:=g*j+i*o+p*l+(a)+h*j+k;\n\tc:=(a*b+i)+p*p+h+j+k+i+y*u/r;\n\tRETURN a+b;\n\tEND\n".as_bytes());
        assert!(result.to_result().is_ok())
    }
}
