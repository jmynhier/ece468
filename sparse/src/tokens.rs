/// Scanner structs for Micro compiler written for Purdue ECE 46800 Fall 2017.
/// JWM

use std::fmt;

/// Holds one Micro token.
/// Selects between the different categories of tokens. Is
/// output by the scanner.
#[derive(PartialEq, Debug, Clone)]
pub enum UToken {
    Identifier(String),
    IntLiteral(String),
    FloatLiteral(String),
    StringLiteral(String),
    Comment,
    Keyword(UTKeyword),
    Operator(UTOperator),
}

impl fmt::Display for UToken {
    /// Println formatting for UToken. Used by Step1 grading script.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            UToken::Identifier(ref x) => write!(f, "{}", x.to_string()),
            UToken::IntLiteral(ref x) => write!(f, "{}", x.to_string()),
            UToken::FloatLiteral(ref x) => write!(f, "{}", x.to_string()),
            UToken::StringLiteral(ref x) => write!(f, "{}", x.to_string()),
            UToken::Comment => write!(f, ""),
            UToken::Keyword(ref x) => write!(f, "{}", x.to_string()),
            UToken::Operator(ref x) => write!(f, "{}", x.to_string()),
        }
    }
}

/// Selects between Micro keywords. Used by UToken.
#[derive(PartialEq, Debug, Clone)]
pub enum UTKeyword {
    Program,
    Begin,
    End,
    Function,
    Read,
    Write,
    If,
    Else,
    Fi,
    For,
    Rof,
    Return,
    Int,
    Void,
    String_,
    Float,
}

impl fmt::Display for UTKeyword {
    /// Println formatting for UTKeyword. Used by UToken::Display.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "{}",
               match *self {
                   UTKeyword::Program => "PROGRAM",
                   UTKeyword::Begin => "BEGIN",
                   UTKeyword::End => "END",
                   UTKeyword::Function => "FUNCTION",
                   UTKeyword::Read => "READ",
                   UTKeyword::Write => "WRITE",
                   UTKeyword::If => "IF",
                   UTKeyword::Else => "ELSE",
                   UTKeyword::Fi => "FI",
                   UTKeyword::For => "FOR",
                   UTKeyword::Rof => "ROF",
                   UTKeyword::Return => "RETURN",
                   UTKeyword::Int => "INT",
                   UTKeyword::Void => "VOID",
                   UTKeyword::String_ => "STRING",
                   UTKeyword::Float => "FLOAT",
               })
    }
}

/// Selects between Micro operators. Used by UToken.
#[derive(PartialEq, Debug, Clone)]
pub enum UTOperator {
    Assign,
    Add,
    Sub,
    Mult,
    Div,
    Equal,
    NotEqual,
    LT,
    GT,
    OpenPar,
    ClosePar,
    Semi,
    Comma,
    LE,
    GE,
}

impl fmt::Display for UTOperator {
    /// Println formatting for UTOperator. Used by UToken::Display.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "{}",
               match *self {
                   UTOperator::Assign => ":=",
                   UTOperator::Add => "+",
                   UTOperator::Sub => "-",
                   UTOperator::Mult => "*",
                   UTOperator::Div => "/", 
                   UTOperator::Equal => "=",
                   UTOperator::NotEqual => "!=",
                   UTOperator::LT => "<",
                   UTOperator::GT => ">",
                   UTOperator::OpenPar => "(",
                   UTOperator::ClosePar => ")",
                   UTOperator::Semi => ";",
                   UTOperator::Comma => ",",
                   UTOperator::LE => "<=",
                   UTOperator::GE => ">=",
               })
    }
}
