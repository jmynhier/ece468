/// Implementation of the Abstract Syntax Tree (AST).
/// Walks the parse tree, and holds the symbol table to build
/// the AST. Need more documentation.

use sparse::blocks::*;
use sparse::symbols::*;
use std::collections::HashMap;

#[derive(Debug)]
pub struct AST {
    pub symbols: HashMap<String, SymbolTable>,
    pub functions: Vec<Function>,
    pub error: Option<ASTErr>,
}

impl AST {
    pub fn new(symbols: HashMap<String, SymbolTable>) -> AST {
        AST {
            symbols: symbols,
            functions: Vec::new(),
            error: None,
        }
    }

    /// Travel from the current scope upward to find variable name.
    /// If you find it, return its type, otherwise return None.
    fn fetch_var(&mut self, var_name: String, mut scope: String) -> Option<UTKeyword> {
        loop {
            match self.symbols.get(&scope) {
                // Fetch the correct symbol table.
                Some(table) => {
                    // Search for the variable.
                    match table.symbols.get(&var_name) {
                        Some(entry) => return Some(entry.type_name.clone()),
                        // Try to go to the parent scope.
                        None => {
                            if let Some(ref parent) = table.parent {
                                // Check your params.
                                match table.params.get(&var_name) {
                                    Some(entry) => return Some(entry.type_name.clone()),
                                    // Move up to parent scope.
                                    None => scope = parent.clone(),
                                }
                            } else {
                                // Checked all the way up to GLOBAL with no match.
                                self.error = Some(ASTErr::VarOutOfScope(var_name.clone()));
                                break;
                            }
                        }
                    }
                }
                // Should be unreachable.
                None => break,
            }
        }
        None
    }

    /// Search for a function and get its return type.
    fn fetch_fn(&mut self, fn_name: String) -> Option<UTKeyword> {
        match self.symbols.get(&fn_name) {
            Some(table) => table.return_type.clone(),
            None => None,
        }
    }

    /// Start point to making the AST.
    pub fn construct(&mut self, mut program: Program) {
        self.functions = program
            .p_body
            .func_declarations
            .func_declarations
            .iter_mut()
            .map(|func| self.function_(func))
            .collect::<Vec<_>>();
    }

    fn function_(&mut self, function_: &mut FuncDecl) -> Function {
        let name = get_name(&function_.id);
        Function {
            name: name.clone(),
            statements: function_
                .func_body
                .stmt_list
                .stmts
                .iter_mut()
                .map(|stmt| self.statement(stmt, name.clone()))
                .collect::<Vec<_>>(),
        }
    }

    fn statement(&mut self, statement: &mut Stmt, scope: String) -> Statement {
        match *statement {
            Stmt::SBaseStmt(ref mut base) => {
                match base {
                    &mut BaseStmt::BAssignStmt(ref mut assign) => {
                        Statement::Assign(self.assign(&mut assign.assign_expr, scope))
                    }
                    &mut BaseStmt::BReadStmt(ref mut read) => Statement::Read(self.read(read, scope)),
                    &mut BaseStmt::BWriteStmt(ref mut write) => {
                        Statement::Write(self.write(write, scope))
                    }
                    &mut BaseStmt::BReturnStmt(ref mut return_) => {
                        Statement::Return(self.return_(return_, scope))
                    }
                }
            }
            Stmt::SIfStmt(ref mut if_) => Statement::If(self.if_(if_, scope)),
            Stmt::SForStmt(ref mut for_) => Statement::For(self.for_(for_, scope)),
        }
    }

    fn assign(&mut self, assign: &mut AssignExpr, scope: String) -> Assign {
        Assign {
            name: get_name(&assign.id),
            exp: Box::new(self.exp(&mut assign.expr, scope)),
        }
    }

    fn read(&mut self, read: &mut ReadStmt, scope: String) -> Read {
        Read {
            exps: read
                .expr_list
                .exprs
                .iter_mut()
                .map(|postfix_expr| Box::new(self.exp(postfix_expr, scope.clone())))
                .collect::<Vec<_>>(),
        }
    }

    fn write(&mut self, write: &mut WriteStmt, scope: String) -> Write {
        Write {
            exps: write
                .expr_list
                .exprs
                .iter_mut()
                .map(|postfix_expr| Box::new(self.exp(postfix_expr, scope.clone()))) 
                .collect::<Vec<_>>(),
        }
    }

    fn return_(&mut self, return_: &mut ReturnStmt, scope: String) -> Return {
        Return { exp: Box::new(self.exp(&mut return_.expr, scope)) }
    }

    fn exp(&mut self, exp: &mut Expr, scope: String) -> Exp {
        if exp.expr_prefixes.len() == 0 {
            return self.factor(&mut exp.factor, scope);
        }

        let rhs = self.factor(&mut exp.factor, scope.clone());

        let prefix = exp.expr_prefixes.pop().unwrap();
        let op = prefix.addop.op.clone();
        exp.factor = prefix.factor;

        let lhs = self.exp(exp, scope);
        let r_type = rhs.get_type();

        if r_type != lhs.get_type() {
            self.error = Some(ASTErr::TypeMismatch(format!("Type fail: {:?} and {:?}", lhs, rhs)));
        }

        match op {
            UTOperator::Add => {
                Exp::AddExp(AddExp {
                                lhs: Box::new(lhs),
                                rhs: Box::new(rhs),
                                return_type: r_type,
                            })
            }
            _ => {
                Exp::SubExp(SubExp {
                                lhs: Box::new(lhs),
                                rhs: Box::new(rhs),
                                return_type: r_type,
                            })
            }
        }

    }

    fn factor(&mut self, mut factor: &mut Factor, scope: String) -> Exp {
        if factor.factor_prefixes.len() == 0 {
            return self.postfix(&mut factor.postfix_expr, scope);
        }

        let rhs = self.postfix(&mut factor.postfix_expr, scope.clone());

        let prefix = factor.factor_prefixes.pop().unwrap();
        let FactorPrefix {
            postfix_expr: postfix,
            mulop: Mulop { op },
        } = prefix;
        //let op = prefix.mulop.op.clone();
        factor.postfix_expr = Box::new(postfix);
        let lhs = self.factor(&mut factor, scope);
        let r_type = rhs.get_type();

        if r_type != lhs.get_type() {
            self.error = Some(ASTErr::TypeMismatch(format!("Type fail: {:?} and {:?}", lhs, rhs)));
        }

        match op {
            UTOperator::Mult => {
                Exp::MulExp(MulExp {
                                lhs: Box::new(lhs),
                                rhs: Box::new(rhs),
                                return_type: r_type,
                            })
            }
            _ => {
                Exp::DivExp(DivExp {
                                lhs: Box::new(lhs),
                                rhs: Box::new(rhs),
                                return_type: r_type,
                            })
            }
        }
    }

    fn postfix(&mut self, postfix: &mut PostfixExpr, scope: String) -> Exp {
        match *postfix {
            PostfixExpr::PPrimary(ref mut x) => {
                match *x {
                    Primary::PExpr(ref mut y) => self.exp(y, scope),
                    Primary::PID(ref y) => {
                        Exp::VarRef(VarRef {
                                        name: get_name(y),
                                        return_type: match self.fetch_var(get_name(y), scope) {
                                            Some(r_type) => r_type,
                                            None => UTKeyword::Void,
                                        },
                                    })
                    }
                    Primary::PIntLiteral(ref y) => {
                        Exp::IntLiteral(IntLiteral { value: get_literal(y) })
                    }
                    Primary::PFloatLiteral(ref y) => {
                        Exp::FloatLiteral(FloatLiteral { value: get_literal(y) })
                    }
                }
            }
            PostfixExpr::PCallExpr(ref mut x) => self.call(x, scope),
        }
    }

    fn call(&mut self, call: &mut CallExpr, scope: String) -> Exp {
        Exp::Call(Call {
                      name: get_name(&call.id),
                      return_type: self.fetch_fn(get_name(&call.id)).unwrap(),
                      exps: match call.expr_list {
                          Some(ref mut expr_list) => {
                              expr_list
                                  .exprs
                                  .iter_mut()
                                  .map(|expr| Box::new(self.exp(expr, scope.clone())))
                                  .collect::<Vec<_>>()
                          }
                          None => Vec::new(),
                      },
                  })
    }

    /// Placeholder
    fn for_(&mut self, for_: &mut ForStmt, parent_scope: String) -> For {
        let scope = for_.scope_name.clone();
        For {
            init: match for_.init_stmt.assign_expr {
                Some(ref mut assign) => Some(self.assign(assign, parent_scope)),
                None => None,
            },
            cond: self.cmp_expr(&mut for_.cond, scope.clone()),
            incr: match for_.incr_stmt.assign_expr {
                Some(ref mut assign) => Some(self.assign(assign, scope.clone())),
                None => None,
            },
            statements: for_.stmt_list
                .stmts
                .iter_mut()
                .map(|stmt| self.statement(stmt, scope.clone()))
                .collect::<Vec<_>>(),
            scope_name: scope,
        }
    }

    fn if_(&mut self, if_: &mut IfStmt, parent_scope: String) -> If {
        let scope = if_.scope_name.clone();

        If {
            cond: self.cmp_expr(&mut if_.cond, parent_scope),
            statements: if_.stmt_list
                .stmts
                .iter_mut()
                .map(|stmt| self.statement(stmt, scope.clone()))
                .collect::<Vec<_>>(),
            else_part: match if_.else_part {
                Some(ref mut else_part) => Some(self.else_(else_part)),
                None => None,
            },
            scope_name: if_.scope_name.clone(),
        }
    }

    fn else_(&mut self, else_: &mut ElsePart) -> Else {
        let scope = else_.scope_name.clone();
        Else {
            statements: match else_.stmt_list {
                Some(ref mut stmt_list) => {
                    stmt_list
                        .stmts
                        .iter_mut()
                        .map(|stmt| self.statement(stmt, scope.clone()))
                        .collect::<Vec<_>>()
                }
                None => Vec::new(),
            },
            scope_name: else_.scope_name.clone(),
        }
    }

    fn cmp_expr(&mut self, cmp_expr: &mut Cond, scope: String) -> CmpExp {
        let exp1 = self.exp(&mut cmp_expr.expr1, scope.clone());
        let exp2 = self.exp(&mut cmp_expr.expr2, scope.clone());

        if exp1.get_type() != exp2.get_type() && self.error.is_none() {
            self.error =
                Some(ASTErr::TypeMismatch(format!("Type fail: {:?} and {:?}", exp1, exp2)));
        }

        CmpExp {
            op: cmp_expr.compop.op.clone(),
            return_type: exp1.get_type(),
            lhs: Box::new(exp1),
            rhs: Box::new(exp2),
        }
    }
}

/// Helper function to extract id names.
fn get_name(id: &ID) -> String {
    match id.identifier {
        UToken::Identifier(ref x) => x.clone(),
        _ => "ERR".to_string(),
    }
}

/// Helper function to extract literal values.
fn get_literal(token: &UToken) -> String {
    match token {
        &UToken::IntLiteral(ref value) => value.clone(),
        &UToken::FloatLiteral(ref value) => value.clone(),
        &UToken::StringLiteral(ref value) => value.clone(),
        _ => "ERR".to_string(),
    }
}

#[derive(Debug)]
pub enum ASTErr {
    VarOutOfScope(String),
    TypeMismatch(String),
}

#[derive(Debug)]
pub struct VarRef {
    pub name: String,
    pub return_type: UTKeyword,
}

#[derive(Debug)]
pub struct IntLiteral {
    pub value: String,
}

#[derive(Debug)]
pub struct FloatLiteral {
    pub value: String,
}

#[derive(Debug)]
pub struct Function {
    pub name: String,
    pub statements: Vec<Statement>,
}

#[derive(Debug)]
pub enum Statement {
    Assign(Assign),
    Read(Read),
    Write(Write),
    Return(Return),
    If(If),
    For(For),
}

#[derive(Debug)]
pub struct Assign {
    pub name: String,
    pub exp: Box<Exp>,
}

#[derive(Debug)]
pub struct Read {
    pub exps: Vec<Box<Exp>>,
}

#[derive(Debug)]
pub struct Write {
    pub exps: Vec<Box<Exp>>,
}

#[derive(Debug)]
pub struct Return {
    pub exp: Box<Exp>,
}

#[derive(Debug)]
pub struct If {
    pub cond: CmpExp,
    pub statements: Vec<Statement>,
    pub else_part: Option<Else>,
    pub scope_name: String,
}

#[derive(Debug)]
pub struct Else {
    pub statements: Vec<Statement>,
    pub scope_name: String,
}

#[derive(Debug)]
pub struct For {
    pub init: Option<Assign>,
    pub cond: CmpExp,
    pub incr: Option<Assign>,
    pub statements: Vec<Statement>,
    pub scope_name: String,
}

#[derive(Debug)]
pub enum Exp {
    Call(Call),
    //CmpExp(CmpExp),
    AddExp(AddExp),
    SubExp(SubExp),
    MulExp(MulExp),
    DivExp(DivExp),
    IntLiteral(IntLiteral),
    FloatLiteral(FloatLiteral),
    VarRef(VarRef),
}

impl Exp {
    fn get_type(&self) -> UTKeyword {
        match *self {
            Exp::Call(ref x) => x.return_type.clone(),
            //Exp::CmpExp(ref x) => x.return_type.clone(),
            Exp::AddExp(ref x) => x.return_type.clone(),
            Exp::SubExp(ref x) => x.return_type.clone(),
            Exp::MulExp(ref x) => x.return_type.clone(),
            Exp::DivExp(ref x) => x.return_type.clone(),
            Exp::IntLiteral(_) => UTKeyword::Int,
            Exp::FloatLiteral(_) => UTKeyword::Float,
            Exp::VarRef(ref x) => x.return_type.clone(),
        }
    }
}

#[derive(Debug)]
pub struct Call {
    pub name: String,
    pub return_type: UTKeyword,
    pub exps: Vec<Box<Exp>>,
}

#[derive(Debug)]
pub struct CmpExp {
    pub op: UTOperator,
    pub return_type: UTKeyword,
    pub lhs: Box<Exp>,
    pub rhs: Box<Exp>,
}

#[derive(Debug)]
pub struct AddExp {
    pub return_type: UTKeyword,
    pub lhs: Box<Exp>,
    pub rhs: Box<Exp>,
}

#[derive(Debug)]
pub struct SubExp {
    pub return_type: UTKeyword,
    pub lhs: Box<Exp>,
    pub rhs: Box<Exp>,
}

#[derive(Debug)]
pub struct MulExp {
    pub return_type: UTKeyword,
    pub lhs: Box<Exp>,
    pub rhs: Box<Exp>,
}

#[derive(Debug)]
pub struct DivExp {
    pub return_type: UTKeyword,
    pub lhs: Box<Exp>,
    pub rhs: Box<Exp>,
}
