/// Three address code generation.
/// JWM

pub use ast::*;
use sparse::tokens::{UTKeyword, UTOperator};
use sparse::SymbolTable;
use std::fmt::{self, Display};
use std::collections::HashMap;

/// 3AC commands.
#[derive(Hash, Clone, Debug, PartialEq)]
pub enum Command {
    ADDI,
    SUBI,
    MULI,
    DIVI,
    ADDF,
    SUBF,
    MULF,
    DIVF,
    STOREI,
    STOREF,
    READI,
    READF,
    WRITEI,
    WRITEF,
    WRITES,
    INCRI,
    DECRI,
    JMPE,
    JMPNE,
    JMPGT,
    JMPLT,
    JMPGE,
    JMPLE,
    JMP,
    LABEL,
    CMPI,
    CMPF,
    PUSH,
    POP,
    LINK,
    UNLINK,
    HALT,
    JSR,
    RET,
}

impl Display for Command {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Command::ADDI => write!(f, "ADDI"),
            Command::SUBI => write!(f, "SUBI"),
            Command::MULI => write!(f, "MULI"),
            Command::DIVI => write!(f, "DIVI"),
            Command::ADDF => write!(f, "ADDF"),
            Command::SUBF => write!(f, "SUBF"),
            Command::MULF => write!(f, "MULF"),
            Command::DIVF => write!(f, "DIVF"),
            Command::STOREI => write!(f, "STOREI"),
            Command::STOREF => write!(f, "STOREF"),
            Command::READI => write!(f, "READI"),
            Command::READF => write!(f, "READF"),
            Command::WRITEI => write!(f, "WRITEI"),
            Command::WRITEF => write!(f, "WRITEF"),
            Command::WRITES => write!(f, "WRITES"),
            Command::INCRI => write!(f, "INCRI"),
            Command::DECRI => write!(f, "DECRI"),
            Command::JMPE => write!(f, "JMPE"),
            Command::JMPNE => write!(f, "JMPNE"),
            Command::JMPGT => write!(f, "JMPGT"),
            Command::JMPLT => write!(f, "JMPLT"),
            Command::JMPGE => write!(f, "JMPGE"),
            Command::JMPLE => write!(f, "JMPLE"),
            Command::JMP => write!(f, "JMP"),
            Command::LABEL => write!(f, "LABEL"),
            Command::CMPI => write!(f, "CMPI"),
            Command::CMPF => write!(f, "CMPF"),
            Command::PUSH => write!(f, "PUSH"),
            Command::POP => write!(f, "POP"),
            Command::LINK => write!(f, "LINK"),
            Command::UNLINK => write!(f, "UNLINK"),
            Command::HALT => write!(f, "HALT"),
            Command::JSR => write!(f, "JSR"),
            Command::RET => write!(f, "RET"),
        }
    }
}

/// Store an argument.
#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub enum Arg {
    Name(String),
    Offset(i32),
    Temp(u32),
    Reg(u32),
    Literal(String),
    Empty,
}

impl Display for Arg {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Arg::Name(ref val) => write!(f, "{}", val),
            Arg::Offset(ref val) => write!(f, "${}", val),
            Arg::Temp(ref val) => write!(f, "!{}", val),
            Arg::Reg(ref val) => write!(f, "r{}", val),
            Arg::Literal(ref val) => write!(f, "{}", val),
            Arg::Empty => write!(f, ""),
        }
    }
}

impl Arg {
    pub fn is_name(&self) -> bool {
        match *self {
            Arg::Name(_) => true,
            _ => false,
        }
    }

    pub fn is_offset(&self) -> bool {
        match *self {
            Arg::Offset(_) => true,
            _ => false,
        }
    }

    pub fn is_temp(&self) -> bool {
        match *self {
            Arg::Temp(_) => true,
            _ => false,
        }
    }

    pub fn is_reg(&self) -> bool {
        match *self {
            Arg::Reg(_) => true,
            _ => false,
        }
    }

    pub fn is_literal(&self) -> bool {
        match *self {
            Arg::Literal(_) => true,
            _ => false,
        }
    }

    pub fn is_empty(&self) -> bool {
        match *self {
            Arg::Empty => true,
            _ => false,
        }
    }
}

/// One 3AC instruction.
#[derive(Hash, Debug, Clone)]
pub struct IRNode {
    pub command: Command,
    pub op1: Arg,
    pub op2: Arg,
    pub result: Arg,
}

impl Display for IRNode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.command {
            Command::STOREI | Command::STOREF | Command::INCRI | Command::DECRI => {
                write!(f, "{} {} {}", self.command, self.op1, self.result)
            }
            Command::READI | Command::READF => write!(f, "{} {}", self.command, self.result),
            Command::POP | Command::JSR | Command::LINK | Command::PUSH | Command::JMPE |
            Command::JMPNE | Command::JMPGT | Command::JMPLT | Command::JMPGE |
            Command::JMPLE | Command::JMP | Command::WRITEI | Command::WRITEF |
            Command::WRITES | Command::LABEL => write!(f, "{} {}", self.command, self.op1),
            Command::RET | Command::UNLINK | Command::HALT => write!(f, "{}", self.command),
            _ => {
                write!(f,
                       "{} {} {} {}",
                       self.command,
                       self.op1,
                       self.op2,
                       self.result)
            }
        }
    }
}

/// Group of 3AC instructions.
pub struct CodeObject {
    pub result: Arg,
    pub return_type: UTKeyword,
    pub constant: bool,
    pub lines: Vec<IRNode>,
}

/// Track if a variable is constant, and what its constant is.
#[derive(Debug, Clone)]
struct VarInfo {
    pub is_const: bool,
    pub value: String,
    pub new_name: Arg,
}

/// 3AC wrapper.
pub struct Code {
    // instructions holds lines for one function and its
    // next offset on the stack.
    pub instructions: Vec<(Vec<IRNode>, i32)>,
    var_map: HashMap<String, VarInfo>,
    counter: u32,
    symbols: HashMap<String, SymbolTable>,
}

impl Display for Code {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for instruction in self.instructions.iter().flat_map(|&(ref x, _)| x) {
            let _ = write!(f, ";{}\n", instruction);
        }
        write!(f, "")
    }
}

impl Code {
    pub fn new() -> Code {
        Code {
            instructions: Vec::new(),
            var_map: HashMap::new(),
            counter: 0,
            symbols: HashMap::new(),
        }
    }

    pub fn construct(&mut self, syn_tree: &AST, symbol_table: &HashMap<String, SymbolTable>) {
        self.symbols = symbol_table.clone();

        for symbol in symbol_table.get("GLOBAL").unwrap().symbols.values() {
            if symbol.type_name != UTKeyword::String_ {
                self.var_map
                    .insert(symbol.var_name.clone(),
                            VarInfo {
                                is_const: true,
                                value: "".to_string(),
                                new_name: Arg::Name(symbol.var_name.clone()),
                            });
            }
        }

        let global_map = self.var_map.clone();

        // Lines of code are grouped by function. This vector
        // holds the code that runs before main.
        let mut first_lines = Vec::new();

        // Create the start of the stack.
        for _ in 0..5 {
            first_lines.push(IRNode {
                                 command: Command::PUSH,
                                 op1: Arg::Empty,
                                 op2: Arg::Empty,
                                 result: Arg::Empty,
                             });
        }

        // Jump to main.
        first_lines.push(IRNode {
                             command: Command::JSR,
                             op1: Arg::Name("main".to_string()),
                             op2: Arg::Empty,
                             result: Arg::Empty,
                         });

        // Halt.
        first_lines.push(IRNode {
                             command: Command::HALT,
                             op1: Arg::Empty,
                             op2: Arg::Empty,
                             result: Arg::Empty,
                         });

        for function in syn_tree.functions.iter() {
            let func_lines = self.function(function);

            self.instructions.push(func_lines);

            self.var_map = global_map.clone();
        }

        // Put the pre-main code last so you can pop it first
        // later.
        self.instructions.push((first_lines, 0));
    }

    fn get_local_var_number(&self, scope: &str) -> usize {
        match self.symbols.get(scope) {
            Some(ref table) => {
                let mut count = table.symbols.len();
                for child in &table.children {
                    count += self.get_local_var_number(child);
                }
                count
            }
            _ => 0,
        }
    }

    /// Get the unsigned offset below the frame pointer
    /// where the return value of `scope` function is alloc'd.
    /// These values should be used as positive offsets.
    fn return_val_offset(&self, scope: &str) -> Option<i32> {
        match self.symbols.get(scope) {
            Some(ref table) => {
                let num_params = table.params.len() as i32;
                match table.return_type {
                    Some(ref value) => {
                        match value {
                            // No return value
                            &UTKeyword::Void => None,
                            _ => {
                                // 4 saved regs + params + return addr.
                                Some(4 + num_params + 2)
                            }
                        }
                    }
                    // Should be unreachable.
                    None => None,
                }
            }
            _ => None,
        }
    }

    /// Construct a function level variable mapping.
    /// Global variables mapped by name before starting.
    /// Get #offset for parameters.
    /// Get #-offset for local variables for this
    /// scope and all children scopes.
    fn scope_map(&mut self, mut next: i32, scope: &str) -> i32 {
        let mut children = Vec::new();
        match self.symbols.get(scope) {
            Some(ref table) => {
                let mut param_count = 2; // old fp is 1
                for param in table.param_order.iter().rev() {
                    self.var_map
                        .insert(param.clone(),
                                VarInfo {
                                    is_const: false,
                                    value: "".to_string(),
                                    new_name: Arg::Offset(param_count.clone()),
                                });
                    param_count += 1;
                }

                for variable in table.symbols.keys() {
                    self.var_map
                        .insert(variable.clone(),
                                VarInfo {
                                    is_const: true,
                                    value: "".to_string(),
                                    new_name: Arg::Offset(next.clone()),
                                });
                    next -= 1;
                }

                children.extend(table.children.clone());
            }
            _ => (),
        };

        for child in children {
            next = self.scope_map(next, &child);
        }

        next
    }

    fn function(&mut self, function: &Function) -> (Vec<IRNode>, i32) {
        // Label yourself.
        let mut func_lines = vec![IRNode {
                                      command: Command::LABEL,
                                      op1: Arg::Name(function.name.clone()),
                                      op2: Arg::Empty,
                                      result: Arg::Empty,
                                  }];

        // Get your number of local variables and link.
        let num_local = self.get_local_var_number(&function.name) + 1;
        func_lines.push(IRNode {
                            command: Command::LINK,
                            op1: Arg::Name(num_local.to_string()),
                            op2: Arg::Empty,
                            result: Arg::Empty,
                        });

        // Construct the variable mapping
        let _ = self.scope_map(-1, &function.name);

        // Execute your statements.
        for statement in &function.statements {
            let lines = self.statement(statement, &function.name);
            func_lines.extend(lines);
        }

        (func_lines, -1 * (num_local as i32))
    }

    fn statement(&mut self, statement: &Statement, scope: &str) -> Vec<IRNode> {
        match *statement {
            Statement::Assign(ref assign) => self.assign(assign),
            Statement::Read(ref read) => self.read(read),
            Statement::Write(ref write) => self.write(write),
            Statement::Return(ref return_) => self.return_(return_, scope),
            Statement::If(ref if_) => self.if_(if_, scope),
            Statement::For(ref for_) => self.for_(for_, scope),
        }
    }

    /// Helper that takes care of mapping local variable
    /// names to their frame pointer offset values.
    fn get_name(&mut self, name: &str) -> Arg {
        match self.var_map.get(name) {
            Some(ref entry) => entry.new_name.clone(),
            _ => Arg::Name(name.to_string()),
        }
    }

    fn assign(&mut self, assign: &Assign) -> Vec<IRNode> {
        let name = self.get_name(&assign.name);
        let mut rhs = self.exp(&assign.exp);

        let mut entry = self.var_map.get_mut(&assign.name).unwrap();
        // If RHS and LHS are const, just track the const value
        // without storing it.
        entry.is_const = false;

        rhs.lines
            .push(IRNode {
                      command: match rhs.return_type {
                          UTKeyword::Int => Command::STOREI,
                          _ => Command::STOREF,
                      },
                      op1: rhs.result,
                      op2: Arg::Empty,
                      result: name,
                  });
        rhs.lines
    }

    fn read(&mut self, read: &Read) -> Vec<IRNode> {
        let mut result = Vec::new();
        let mut args = Vec::new();
        
        for exp in read.exps.iter() {
            let sub_exps = self.exp(exp);

            result.extend(sub_exps.lines.clone());
            args.push((sub_exps.result, sub_exps.return_type));
        }

        for (id, r_type) in args {
            result.push(IRNode {
                            command: match r_type {
                                UTKeyword::Int => Command::READI,
                                _ => Command::READF,
                            },
                            op1: Arg::Empty,
                            op2: Arg::Empty,
                            result: id,
                        });
        }

        result
    }

    fn write(&mut self, write: &Write) -> Vec<IRNode> {
        let mut result = Vec::new();
        let mut args = Vec::new();
        
        for exp in write.exps.iter() {
            let sub_exps = self.exp(exp);

            result.extend(sub_exps.lines.clone());
            args.push((sub_exps.result, sub_exps.return_type));
        }

        for (id, r_type) in args {
            result.push(IRNode {
                            command: match r_type {
                                UTKeyword::Int => Command::WRITEI,
                                UTKeyword::Float => Command::WRITEF,
                                _ => Command::WRITES,
                            },
                            op1: id,
                            op2: Arg::Empty,
                            result: Arg::Empty,
                        });
        }
        result
    }

    fn return_(&mut self, return_: &Return, scope: &str) -> Vec<IRNode> {
        let mut result = Vec::new();
        let exp = self.exp(&return_.exp);

        result.extend(exp.lines.clone());

        // If there is a return value, move it.
        match self.return_val_offset(scope) {
            Some(name) => {
                result.push(IRNode {
                                command: Command::STOREI,
                                op1: exp.result,
                                op2: Arg::Empty,
                                result: Arg::Offset(name),
                            });
            }
            None => (),
        }

        // unlink
        result.push(IRNode {
                        command: Command::UNLINK,
                        op1: Arg::Empty,
                        op2: Arg::Empty,
                        result: Arg::Empty,
                    });

        // return
        result.push(IRNode {
                        command: Command::RET,
                        op1: Arg::Empty,
                        op2: Arg::Empty,
                        result: Arg::Empty,
                    });

        result
    }

    fn exp(&mut self, exp: &Box<Exp>) -> CodeObject {
        match **exp {
            Exp::Call(ref x) => self.call(x),
            //Exp::CmpExp(ref x) => self.cmp_exp(x),
            Exp::AddExp(ref x) => self.add_exp(x),
            Exp::SubExp(ref x) => self.sub_exp(x),
            Exp::MulExp(ref x) => self.mul_exp(x),
            Exp::DivExp(ref x) => self.div_exp(x),
            Exp::IntLiteral(ref x) => self.int_lit(x),
            Exp::FloatLiteral(ref x) => self.float_lit(x),
            Exp::VarRef(ref x) => self.var_ref(x),
        }
    }

    fn call(&mut self, call: &Call) -> CodeObject {
        let mut lines = Vec::new();

        // Do any needed calcluations for the call.
        let mut parameter_objects = Vec::new();
        for ref exp in call.exps.iter() {
            let CodeObject {
                result: param_name,
                return_type: _,
                constant: _,
                lines: exp_lines,
            } = self.exp(exp);
            parameter_objects.push(param_name);
            lines.extend(exp_lines)
        }

        //        let scope = self.symbols.get(&call.name).unwrap();

        // Push a blank for the return value if needed.
        if self.return_val_offset(&call.name).is_some() {
            lines.push(IRNode {
                           command: Command::PUSH,
                           op1: Arg::Empty,
                           op2: Arg::Empty,
                           result: Arg::Empty,
                       });
        }

        // Save the registers.
        for i in 0..4 {
            lines.push(IRNode {
                           command: Command::PUSH,
                           op1: Arg::Reg(i),
                           op2: Arg::Empty,
                           result: Arg::Empty,
                       });
        }

        // Push the parameters if needed.
        for res in &parameter_objects {
            lines.push(IRNode {
                           command: Command::PUSH,
                           op1: res.clone(),
                           op2: Arg::Empty,
                           result: Arg::Empty,
                       });
        }

        // Jump and save return address.
        lines.push(IRNode {
                       command: Command::JSR,
                       op1: Arg::Name(call.name.clone()),
                       op2: Arg::Empty,
                       result: Arg::Empty,
                   });

        // Pop the parameters in needed.
        for _ in parameter_objects {
            lines.push(IRNode {
                           command: Command::POP,
                           op1: Arg::Empty,
                           op2: Arg::Empty,
                           result: Arg::Empty,
                       });
        }

        // Pop the registers.
        for i in (0..4).rev() {
            lines.push(IRNode {
                           command: Command::POP,
                           op1: Arg::Reg(i),
                           op2: Arg::Empty,
                           result: Arg::Empty,
                       });
        }

        let mut temp = Arg::Empty;

        // Get the return values if needed.
        if self.return_val_offset(&call.name).is_some() {
            temp = Arg::Temp(self.counter);
            self.counter += 1;
            lines.push(IRNode {
                           command: Command::POP,
                           op1: temp.clone(),
                           op2: Arg::Empty,
                           result: Arg::Empty,
                       });
        }

        CodeObject {
            result: temp,
            return_type: call.return_type.clone(),
            constant: false,
            lines: lines,
        }
    }

    fn add_exp(&mut self, add_exp: &AddExp) -> CodeObject {
        let lhs = self.exp(&add_exp.lhs);
        let rhs = self.exp(&add_exp.rhs);
        let temp = Arg::Temp(self.counter);
        self.counter += 1;

        let mut result = Vec::new();
        result.extend(lhs.lines);
        result.extend(rhs.lines);

        result.push(IRNode {
                        command: match add_exp.return_type {
                            UTKeyword::Int => Command::ADDI,
                            _ => Command::ADDF,
                        },
                        op1: lhs.result,
                        op2: rhs.result,
                        result: temp.clone(),
                    });

        CodeObject {
            result: temp,
            return_type: add_exp.return_type.clone(),
            constant: false,
            lines: result,
        }
    }

    fn sub_exp(&mut self, sub_exp: &SubExp) -> CodeObject {
        let lhs = self.exp(&sub_exp.lhs);
        let rhs = self.exp(&sub_exp.rhs);
        let temp = Arg::Temp(self.counter);
        self.counter += 1;

        let mut result = Vec::new();
        result.extend(lhs.lines);
        result.extend(rhs.lines);

        result.push(IRNode {
                        command: match sub_exp.return_type {
                            UTKeyword::Int => Command::SUBI,
                            _ => Command::SUBF,
                        },
                        op1: lhs.result,
                        op2: rhs.result,
                        result: temp.clone(),
                    });

        CodeObject {
            result: temp,
            return_type: sub_exp.return_type.clone(),
            constant: false,
            lines: result,
        }
    }

    fn mul_exp(&mut self, mul_exp: &MulExp) -> CodeObject {
        let lhs = self.exp(&mul_exp.lhs);
        let rhs = self.exp(&mul_exp.rhs);
        let temp = Arg::Temp(self.counter);
        self.counter += 1;

        let mut result = Vec::new();
        result.extend(lhs.lines);
        result.extend(rhs.lines);

        result.push(IRNode {
                        command: match mul_exp.return_type {
                            UTKeyword::Int => Command::MULI,
                            _ => Command::MULF,
                        },
                        op1: lhs.result,
                        op2: rhs.result,
                        result: temp.clone(),
                    });

        CodeObject {
            result: temp,
            return_type: mul_exp.return_type.clone(),
            constant: false,
            lines: result,
        }
    }

    fn div_exp(&mut self, div_exp: &DivExp) -> CodeObject {
        let lhs = self.exp(&div_exp.lhs);
        let rhs = self.exp(&div_exp.rhs);
        let temp = Arg::Temp(self.counter);
        self.counter += 1;

        let mut result = Vec::new();
        result.extend(lhs.lines);
        result.extend(rhs.lines);

        result.push(IRNode {
                        command: match div_exp.return_type {
                            UTKeyword::Int => Command::DIVI,
                            _ => Command::DIVF,
                        },
                        op1: lhs.result,
                        op2: rhs.result,
                        result: temp.clone(),
                    });

        CodeObject {
            result: temp,
            return_type: div_exp.return_type.clone(),
            constant: false,
            lines: result,
        }
    }

    fn int_lit(&mut self, int_lit: &IntLiteral) -> CodeObject {
        CodeObject {
            result: Arg::Literal(int_lit.value.clone()),
            return_type: UTKeyword::Int,
            constant: false,
            lines: Vec::new(),
        }

    }

    fn float_lit(&mut self, float_lit: &FloatLiteral) -> CodeObject {
        CodeObject {
            result: Arg::Literal(float_lit.value.clone()),
            return_type: UTKeyword::Int,
            constant: false,
            lines: Vec::new(),
        }
    }

    // Only used by exp and children.
    fn var_ref(&mut self, var_ref: &VarRef) -> CodeObject {
        let name = self.get_name(&var_ref.name);

        CodeObject {
            result: name,
            return_type: var_ref.return_type.clone(),
            constant: false,
            lines: Vec::new(),
        }
    }

    /// Moves all tracked const variables into their
    /// memory locations and marks them as non-constant.
    fn var_stash(&mut self) -> Vec<IRNode> {
        let mut result = Vec::new();
        let keys = self.var_map.keys().cloned().collect::<Vec<String>>();
        let mut names = Vec::new();
        for key in keys.iter() {
            names.push(self.get_name(&key));
        }

        for (key, name) in keys.iter().zip(names.iter()) {
            let mut entry = self.var_map.get_mut(key).unwrap();
            if entry.is_const {
                entry.is_const = false;
                if entry.value != "" {
                    result.push(IRNode {
                                    command: Command::STOREI,
                                    op1: Arg::Literal(entry.value.clone()),
                                    op2: Arg::Empty,
                                    result: name.clone(),
                                });
                }
            }
        }
        result
    }

    fn if_(&mut self, if_: &If, f_scope: &str) -> Vec<IRNode> {
        let mut result = self.cond(&if_.cond);

        // For now, store any existing constant variables
        // and mark them as non-constant to avoid trouble.
        result.extend(self.var_stash());

        let command = match if_.cond.op {
            UTOperator::Equal => Command::JMPNE,
            UTOperator::NotEqual => Command::JMPE,
            UTOperator::LT => Command::JMPGE,
            UTOperator::GT => Command::JMPLE,
            UTOperator::LE => Command::JMPGT,
            _ /*GE*/ => Command::JMPLT,
        };

        let end_label = format!("END_{}", &if_.scope_name);

        if let Some(ref else_part) = if_.else_part {
            // Jump to else block.
            result.push(IRNode {
                            command: command,
                            op1: Arg::Name(else_part.scope_name.clone()),
                            op2: Arg::Empty,
                            result: Arg::Empty,
                        });

            // Execute the statements of the THEN block.
            for statement in if_.statements.iter() {
                result.extend(self.statement(statement, f_scope));
            }


            // Jump to the endif.
            result.push(IRNode {
                            command: Command::JMP,
                            op1: Arg::Name(end_label.clone()),
                            op2: Arg::Empty,
                            result: Arg::Empty,
                        });

            // Execute the else block.
            result.extend(self.else_(else_part, f_scope));
        } else {
            // Conditionally jump to endif block.
            result.push(IRNode {
                            command: command,
                            op1: Arg::Name(end_label.clone()),
                            op2: Arg::Empty,
                            result: Arg::Empty,
                        });

            // Execute the statements of the THEN block.
            for statement in if_.statements.iter() {
                result.extend(self.statement(statement, f_scope));
            }

            // Jump to the endif.
            result.push(IRNode {
                            command: Command::JMP,
                            op1: Arg::Name(end_label.clone()),
                            op2: Arg::Empty,
                            result: Arg::Empty,
                        });
        }

        // Label the endif.
        result.push(IRNode {
                        command: Command::LABEL,
                        op1: Arg::Name(end_label),
                        op2: Arg::Empty,
                        result: Arg::Empty,
                    });
        result
    }

    /// Only executes the expressions. Does not jump.
    /// That is up to the caller.
    fn cond(&mut self, cond: &CmpExp) -> Vec<IRNode> {
        let lhs = self.exp(&cond.lhs);
        let rhs = self.exp(&cond.rhs);
        let temp = Arg::Temp(self.counter);
        self.counter += 1;

        let mut lines = lhs.lines.clone();
        lines.extend(rhs.lines);
        lines.push(IRNode {
                       command: match cond.return_type {
                UTKeyword::Int => Command::CMPI,
                _ /*Float*/ => Command::CMPF,
            },
                       op1: lhs.result,
                       op2: rhs.result,
                       result: temp,
                   });
        lines
    }

    fn else_(&mut self, else_: &Else, f_scope: &str) -> Vec<IRNode> {
        let mut result = Vec::new();

        // Label the block.
        result.push(IRNode {
                        command: Command::LABEL,
                        op1: Arg::Name(else_.scope_name.clone()),
                        op2: Arg::Empty,
                        result: Arg::Empty,
                    });

        // Execute the statements.
        for statement in else_.statements.iter() {
            result.extend(self.statement(statement, f_scope));
        }
        result
    }

    fn for_(&mut self, for_: &For, f_scope: &str) -> Vec<IRNode> {

        // For now, store any existing constant variables
        // and mark them as non-constant to avoid trouble.
        let mut result = self.var_stash();

        // Execute the initiaization if it exists.
        if let Some(ref init) = for_.init {
            result.extend(self.assign(init));
        }

        // Mark the head of the check.
        let head_label = format!("{}_HEAD", for_.scope_name);
        result.push(IRNode {
                        command: Command::LABEL,
                        op1: Arg::Name(head_label.clone()),
                        op2: Arg::Empty,
                        result: Arg::Empty,
                    });

        // Compare and jump to end if fail.
        result.extend(self.cond(&for_.cond));

        let command = match for_.cond.op {
            UTOperator::Equal => Command::JMPNE,
            UTOperator::NotEqual => Command::JMPE,
            UTOperator::LT => Command::JMPGE,
            UTOperator::GT => Command::JMPLE,
            UTOperator::LE => Command::JMPGT,
            _ /*GE*/ => Command::JMPLT,
        };

        let end_label = format!("{}_END", for_.scope_name);
        result.push(IRNode {
                        command: command,
                        op1: Arg::Name(end_label.clone()),
                        op2: Arg::Empty,
                        result: Arg::Empty,
                    });

        // Body Statements.
        for statement in for_.statements.iter() {
            result.extend(self.statement(statement, f_scope));
        }

        // Increment.
        if let Some(ref incr) = for_.incr {
            result.extend(self.assign(incr));
        }

        // Jump to comparison.
        result.push(IRNode {
                        command: Command::JMP,
                        op1: Arg::Name(head_label),
                        op2: Arg::Empty,
                        result: Arg::Empty,
                    });


        // Label the end.
        result.push(IRNode {
                        command: Command::LABEL,
                        op1: Arg::Name(end_label),
                        op2: Arg::Empty,
                        result: Arg::Empty,
                    });
        result
    }
}
