/// Tiny code generation from the three address code.
/// JWM

use three_ac::*;
use reg_alloc::Allocator;
use sparse::UTKeyword;
use sparse::SymbolTable;
use std::fmt::{self, Display};
use std::collections::HashMap;
use std::str;

pub struct TinyCode {
    pub code: Vec<String>,
}

impl Display for TinyCode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for code in self.code.iter() {
            let _ = write!(f, "{}\n", code);
        }
        write!(f, "")
    }
}

impl TinyCode {
    pub fn new() -> TinyCode {
        TinyCode { code: Vec::new() }
    }

    /// Entry point for code generation.
    pub fn construct(&mut self,
                     sym_table: &HashMap<String, SymbolTable>,
                     instructions: Allocator) {
        // Just for debugging puposes print the 3AC.
        self.code.push(format!("{}\n", instructions));

        for symbol in sym_table.get("GLOBAL").unwrap().symbols.values() {
            match symbol.type_name {
                UTKeyword::String_ => {
                    self.code
                        .push(format!("str {} {}",
                                      symbol.var_name.clone(),
                                      symbol.string_value.clone().unwrap()));
                }
                // Int or Float
                _ => {
                    self.code.push(format!("var {}", symbol.var_name));
                }
            }
        }

        // Translate each 3AC instruction.
        for node in instructions.lines.iter() {
            match node.command {
                Command::ADDI => {
                    self.code.push(format!("addi {} {}", node.op2, node.op1));
                }
                Command::SUBI => {
                    self.code.push(format!("subi {} {}", node.op2, node.op1));
                }
                Command::MULI => {
                    self.code.push(format!("muli {} {}", node.op2, node.op1));
                }
                Command::DIVI => {
                    self.code.push(format!("divi {} {}", node.op2, node.op1));
                }
                Command::ADDF => {
                    self.code.push(format!("addr {} {}", node.op2, node.op1));
                }
                Command::SUBF => {
                    self.code.push(format!("subr {} {}", node.op2, node.op1));
                }
                Command::MULF => {
                    self.code.push(format!("mulr {} {}", node.op2, node.op1));
                }
                Command::DIVF => {
                    self.code.push(format!("divr {} {}", node.op2, node.op1));
                }
                Command::STOREI | Command::STOREF => {
                    self.code
                        .push(format!("move {} {}", node.op1, node.result));
                }
                Command::READI => {
                    self.code.push(format!("sys readi {}", node.result));
                }
                Command::READF => {
                    self.code.push(format!("sys readr {}", node.result));
                }
                Command::WRITEI => {
                    self.code.push(format!("sys writei {}", node.op1));
                }
                Command::WRITEF => {
                    self.code.push(format!("sys writer {}", node.op1));
                }
                Command::WRITES => {
                    self.code.push(format!("sys writes {}", node.op1));
                }
                Command::INCRI => {
                    //self.regs.insert(node.result.clone(), node.op1);
                }
                Command::DECRI => {
                    //self.regs.insert(node.result.clone(), node.op1);
                }
                Command::JMPE => {
                    self.code.push(format!("jeq {}", node.op1));
                }
                Command::JMPNE => {
                    self.code.push(format!("jne {}", node.op1));
                }
                Command::JMPGT => {
                    self.code.push(format!("jgt {}", node.op1));
                }
                Command::JMPLT => {
                    self.code.push(format!("jlt {}", node.op1));
                }
                Command::JMPGE => {
                    self.code.push(format!("jge {}", node.op1));
                }
                Command::JMPLE => {
                    self.code.push(format!("jle {}", node.op1));
                }
                Command::JMP => {
                    self.code.push(format!("jmp {}", node.op1));
                }
                Command::LABEL => {
                    self.code.push(format!("label {}", node.op1));
                }
                Command::CMPI => {
                    self.code.push(format!("cmpi {} {}", node.op1, node.op2));
                }
                Command::CMPF => {
                    self.code.push(format!("cmpr {} {}", node.op1, node.op2));
                }
                Command::PUSH => {
                    self.code.push(format!("push {}", node.op1));
                }
                Command::POP => {
                    self.code.push(format!("pop {}", node.op1));
                }
                Command::LINK => {
                    self.code.push(format!("link {}", node.op1));
                }
                Command::UNLINK => {
                    self.code.push("unlnk".to_string());
                }
                Command::HALT => {
                    self.code.push("sys halt".to_string());
                }
                Command::JSR => {
                    self.code.push(format!("jsr {}", node.op1));
                }
                Command::RET => {
                    self.code.push("ret".to_string());
                }
            }
        }
    }
}
