use std::collections::{HashMap, HashSet};
use std::fmt::{self, Display};
use three_ac::{IRNode, Arg, Command, Code};

/// Hold all info about an input line.
#[derive(Clone, Debug)]
struct Line {
    node: IRNode,
    previous: HashSet<usize>,
    next: HashSet<usize>,
    kill: HashSet<Arg>,
    gen: HashSet<Arg>,
    in_set: HashSet<Arg>,
    out_set: HashSet<Arg>,
}

impl Line {
    fn new(node: IRNode) -> Line {
        let mut my_line = Line {
            node: node,
            previous: HashSet::new(),
            next: HashSet::new(),
            kill: HashSet::new(),
            gen: HashSet::new(),
            in_set: HashSet::new(),
            out_set: HashSet::new(),
        };

        my_line.analyse();
        my_line
    }

    /// Looks at the node and sets the kill and gen sets accordingly.
    fn analyse(&mut self) {
        self.gen = HashSet::new();
        self.kill = HashSet::new();
        let command = self.node.command.clone();
        match command {
            Command::ADDI | Command::SUBI | Command::MULI | Command::DIVI | Command::ADDF |
            Command::SUBF | Command::MULF | Command::DIVF => {
                self.kill.insert(self.node.result.clone());
                self.gen.insert(self.node.op1.clone());
                self.gen.insert(self.node.op2.clone());
            }
            Command::STOREI | Command::STOREF => {
                self.kill.insert(self.node.result.clone());
                self.gen.insert(self.node.op1.clone());
            }
            Command::READI | Command::READF => {
                self.kill.insert(self.node.result.clone());
            }
            Command::INCRI | Command::DECRI => {
                //DEBUG not currently used.
            }
            Command::JMPE | Command::JMPNE | Command::JMPGT | Command::JMPLT | Command::JMPGE |
            Command::JMPLE | Command::JMP => {
                // Do nothing.
            }
            Command::LABEL | Command::LINK | Command::WRITES | Command::UNLINK | Command::RET |
            Command::HALT => {
                // Also do nothing.
            }
            Command::WRITEI | Command::WRITEF | Command::PUSH => {
                if self.node.op1 != Arg::Empty {
                    self.gen.insert(self.node.op1.clone());
                }
            }
            Command::CMPI | Command::CMPF => {
                self.gen.insert(self.node.op1.clone());
                self.gen.insert(self.node.op2.clone());
            }
            Command::POP => {
                if self.node.op1 != Arg::Empty {
                    self.kill.insert(self.node.op1.clone());
                }
            }
            Command::JSR => {
                // Gen all globals? May need to use symbol table.
            }
        }
    }
}

/// Holds the state for one register.
#[derive(Debug, Clone)]
struct Reg {
    arg: Arg,
    reg: Arg,
    dirty: bool,
}

impl Reg {
    fn new(reg: Arg, arg: Arg, dirty: bool) -> Reg {
        Reg {
            arg: arg,
            reg: reg,
            dirty: dirty,
        }
    }
}

/// Takes three address code with temporaries and no registers
/// and performs 4 register allocation.
#[derive(Clone, Debug)]
pub struct Allocator {
    pub lines: Vec<IRNode>,

    /// State that tracks liveness.
    work_list: HashMap<usize, Line>,
    done_list: HashMap<usize, Line>,

    /// Registers
    free_regs: Vec<Arg>,
    used_regs: HashMap<Arg, Reg>,

    /// Map label to its enumeration.
    label_map: HashMap<Arg, usize>,

    /// Handle temp spillage.
    next_offset: i32,
    temp_map: HashMap<Arg, Arg>, // <Temp, Offset>
}

impl Display for Allocator {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for instruction in self.lines.iter() {
            let _ = write!(f, ";{}\n", instruction);
        }
        write!(f, "")
    }
}

impl Allocator {
    pub fn new() -> Allocator {
        Allocator {
            lines: Vec::new(),
            work_list: HashMap::new(),
            done_list: HashMap::new(),
            label_map: HashMap::new(),
            free_regs: vec![Arg::Reg(0), Arg::Reg(1), Arg::Reg(2), Arg::Reg(3)],
            used_regs: HashMap::new(),
            next_offset: 0,
            temp_map: HashMap::new(),
        }
    }

    pub fn construct(&mut self, mut code: Code) {
        // Assume no register allocation is needed in the
        // pre-main setup code.
        let (lines, _) = code.instructions.pop().unwrap();
        self.lines.extend(lines);

        for (func, offset) in code.instructions.into_iter() {
            self.next_offset = offset;
            self.alloc(func);
            self.clean();
        }
    }

    /// Reset state between functions.
    fn clean(&mut self) {
        self.work_list = HashMap::new();
        self.done_list = HashMap::new();
        self.free_regs = vec![Arg::Reg(0), Arg::Reg(1), Arg::Reg(2), Arg::Reg(3)];
        self.used_regs = HashMap::new();
        self.temp_map = HashMap::new();
    }

    /// Handles 4 register allocation for one function.
    fn alloc(&mut self, func: Vec<IRNode>) {
        // Map jump tagets to their line number.
        for (id, command) in func.iter().enumerate() {
            if command.command == Command::LABEL {
                self.label_map.insert(command.op1.clone(), id);
            }
        }

        // Create the graph with the GEN and KILL sets for each lines
        // and map the relation of each line to its neighbors now
        // that all the labels have been mapped.
        let max = func.len() - 1;
        for (id, command) in func.iter().enumerate() {
            self.graph(id, command, max);
        }

        // Liveness analysis.
        'live: loop {
            if self.work_list.len() == 0 {
                break 'live;
            }

            // Get some entry from the work list.
            //DEBUG: pick the one with the highest key.
            let last_id = self.work_list
                .keys()
                .cloned()
                .into_iter()
                .max()
                .unwrap();
            let mut entry = self.work_list.remove(&last_id).unwrap();

            // Check each of the live in sets of entry's next nodes.
            let entry_updated = self.update(&mut entry);

            if entry_updated {
                // Put all previous nodes on the work list.
                for id in entry.previous.iter() {
                    let parent = self.done_list.remove(id);
                    match parent {
                        Some(node) => {
                            self.work_list.insert(id.clone(), node);
                        }
                        None => (),
                    }
                }
            }
            self.done_list.insert(last_id, entry);
        }

        // Register alocation.
        for i in 0..self.done_list.len() {
            self.allocate(i);
        }

        'relink: for mut line in self.lines.iter_mut().rev() {
            if line.command == Command::LINK {
                line.op1 = Arg::Literal(format!("{}", (self.next_offset as i32) * -1));
                break 'relink;
            }
        }
    }

    /// For a given line, check the live-in sets of all
    /// lines on its next set. Update accordingly.
    fn update(&mut self, line: &mut Line) -> bool {
        let mut result = false;
        for &id in line.next.iter() {
            // Find the next entry.
            let next = if self.work_list.contains_key(&id) {
                self.work_list.get_mut(&id).unwrap()
            } else {
                self.done_list.get_mut(&id).unwrap()
            };

            // If there are new values in the next in set, return true.
            result |= next.in_set
                .iter()
                .fold(false, |acc, live_id| acc | !line.out_set.contains(live_id));

            // My out set is the union of all the next in sets.
            line.out_set.extend(next.in_set.clone());

            // Construct the expected in set.
            //TODO: is there a less expensive way?
            let in_set = line.out_set
                .difference(&line.kill)
                .into_iter()
                .cloned()
                .collect::<HashSet<_>>()
                .union(&line.gen)
                .into_iter()
                .cloned()
                .collect::<HashSet<_>>();

            if in_set != line.in_set {
                result = true;
                line.in_set = in_set;
            }

        }
        result
    }

    /// Creates the next set for `node` and adds `id` to the
    /// previous set of anything in the next set.
    fn graph(&mut self, id: usize, node: &IRNode, max: usize) {
        let command = node.command.clone();
        let next = if id != max {
            match command {
                Command::JMPE | Command::JMPNE | Command::JMPGT | Command::JMPLT |
                Command::JMPGE | Command::JMPLE => vec![id + 1, self.get_id(&node.op1)],
                Command::RET => Vec::new(),
                Command::JMP => vec![self.get_id(&node.op1)],
                _ => {
                    // Straight line code.
                    vec![id + 1]
                }
            }
        } else {
            Vec::new()
        };

        // Set yourself as previous.
        for target in next.iter().cloned() {
            let mut target_entry = self.work_list
                .entry(target)
                .or_insert(Line::new(node.clone()));
            target_entry.previous.insert(id);
        }

        // Update your own Line.
        let mut entry = self.work_list
            .entry(id)
            .or_insert(Line::new(node.clone()));
        entry.node = node.clone(); // In case it was added earlier as a temp.
        entry.analyse();

        entry.next.extend(next);
    }

    /// Access the label->id mapping.
    fn get_id(&self, arg: &Arg) -> usize {
        self.label_map.get(arg).unwrap().clone()
    }

    /// Using the live-in, live-out sets and current register state,
    /// allocate registers for this line of code.
    /// Since you are iterating backwards, handle any changes to the
    /// registers after you push the new version of the line.
    fn allocate(&mut self, i: usize) {
        let line = self.done_list.remove(&i).unwrap();
        let mut new_code = Vec::new();

        // Kill any active regs that hold a member of the kill set.
        for arg in line.kill.iter() {
            if !line.gen.contains(&arg) {
                let mut reg = Arg::Empty;
                if let Some(entry) = self.used_regs.remove(&arg) {
                    if entry.dirty {
                        new_code.push(IRNode {
                                          command: Command::STOREI,
                                          op1: entry.reg.clone(),
                                          op2: Arg::Empty,
                                          result: arg.clone(),
                                      });
                    }
                    reg = entry.reg.clone();
                }
                if reg.is_reg() {
                    self.free_regs.push(reg);
                }
            }
        }

        // Make sure that all required reads are in regs.
        for arg in line.gen.iter() {
            let (_, new_lines) = self.ensure(arg, &line.gen);
            new_code.extend(new_lines);
        }

        match line.node.command {
            Command::ADDI | Command::SUBI | Command::MULI | Command::DIVI | Command::ADDF |
            Command::SUBF | Command::MULF | Command::DIVF => {
                let mut node = IRNode {
                    command: line.node.command.clone(),
                    op1: self.get_reg(&line.node.op1, &line.gen),
                    op2: self.get_reg(&line.node.op2, &line.gen),
                    result: Arg::Empty,
                };

                // Set up the Reg = Reg + omprl relationship.
                let result = if line.node.op1 == line.node.result {
                    self.get_reg(&line.node.op1, &line.gen)
                } else if !self.is_dirty(&line.node.op1) {
                    let alias = if let Some(offset) = self.temp_map.get(&line.node.op1) {
                        offset.clone()
                    } else {
                        line.node.op1.clone()
                    };

                    if !line.node.op1.is_temp() {
                        new_code.push(IRNode {
                                          command: Command::STOREI,
                                          op1: line.node.op1.clone(),
                                          op2: Arg::Empty,
                                          result: self.get_reg(&line.node.op1, &line.gen),
                                      });
                    }

                    self.swap_state(&alias, &line.node.result, true);
                    self.get_reg(&line.node.result, &line.gen)
                } else if !line.node.op1.is_temp() {
                    new_code.push(IRNode {
                                      command: Command::STOREI,
                                      op1: self.get_reg(&line.node.op1, &line.gen),
                                      op2: Arg::Empty,
                                      result: line.node.op1.clone(),
                                  });
                    self.swap_state(&line.node.op1, &line.node.result, true);
                    self.get_reg(&line.node.result, &line.gen)
                } else {
                    let offset = Arg::Offset(self.next_offset);
                    self.next_offset -= 1;

                    self.temp_map
                        .insert(line.node.op1.clone(), offset.clone());

                    let arg_dirty = self.used_regs
                        .get(&line.node.op1)
                        .unwrap()
                        .dirty
                        .clone();

                    self.swap_state(&line.node.op1, &offset, arg_dirty);

                    if line.out_set.contains(&line.node.op1.clone()) {
                        new_code.push(IRNode {
                                          command: Command::STOREI,
                                          op1: self.get_reg(&offset, &line.gen),
                                          op2: Arg::Empty,
                                          result: offset.clone(),
                                      });
                    }

                    self.swap_state(&offset, &line.node.result, true);
                    self.get_reg(&line.node.result, &line.gen)
                };

                node.result = result;

                self.lines.extend(new_code);
                self.lines.push(node);
            }
            Command::STOREI | Command::STOREF => {
                let mut node = IRNode {
                    command: line.node.command.clone(),
                    op1: self.get_reg(&line.node.op1, &line.gen),
                    op2: Arg::Empty,
                    result: Arg::Empty,
                };

                node.result = self.get_reg(&line.node.result, &line.gen);

                self.lines.extend(new_code);
                self.lines.push(node);
            }
            Command::READI | Command::READF => {
                let node = IRNode {
                    command: line.node.command.clone(),
                    op1: Arg::Empty,
                    op2: Arg::Empty,
                    result: line.node.result.clone(),
                };

                // Kill any existing register.
                if let Some(entry) = self.used_regs.remove(&line.node.result) {
                    self.free_regs.push(entry.reg.clone());
                }
                self.lines.push(node);
            }
            Command::INCRI | Command::DECRI => {
                //DEBUG not currently used.
            }
            Command::JMPE | Command::JMPNE | Command::JMPGT | Command::JMPLT | Command::JMPGE |
            Command::LABEL | Command::JMPLE | Command::JMP | Command::JSR => {
                let removals = self.used_regs
                    .iter()
                    //.filter(|&(arg, _)| !arg.is_offset() && !line.out_set.contains(arg))
                    .map(|(arg, _)| arg.clone())
                    .collect::<Vec<_>>();

                for removal in removals {
                    let record = self.used_regs.remove(&removal).unwrap();
                    if record.dirty && !removal.is_temp() {
                        new_code.push(IRNode {
                                          command: Command::STOREI,
                                          op1: record.reg.clone(),
                                          op2: Arg::Empty,
                                          result: removal.clone(),
                                      });
                    }

                    self.free_regs.push(record.reg);
                }

                self.lines.push(line.node.clone());
                self.lines.extend(new_code);
            }
            Command::LINK | Command::UNLINK | Command::RET | Command::HALT | Command::WRITES => {
                self.lines.push(line.node.clone());
                self.lines.extend(new_code);
            }
            Command::WRITEI | Command::WRITEF | Command::PUSH => {
                let node = if line.node.op1.is_reg() {
                    IRNode {
                        command: line.node.command.clone(),
                        op1: line.node.op1.clone(),
                        op2: Arg::Empty,
                        result: Arg::Empty,
                    }
                } else if !self.is_dirty(&line.node.op1) {
                    IRNode {
                        command: line.node.command.clone(),
                        op1: self.get_reg(&line.node.op1, &line.gen),
                        op2: Arg::Empty,
                        result: Arg::Empty,
                    }
                } else {
                    IRNode {
                        command: line.node.command.clone(),
                        op1: self.get_reg(&line.node.op1, &line.gen),
                        op2: Arg::Empty,
                        result: Arg::Empty,
                    }
                };

                // Make sure that the vaue is really in the register.
                if !line.node.op1.is_reg() && !line.node.op1.is_empty() &&
                        (line.node.op1.is_temp() && self.temp_map.contains_key(&line.node.op1) ||
                         !line.node.op1.is_temp()) {
                    new_code.push(IRNode {
                        command: Command::STOREI,
                        op1: match self.temp_map.get(&line.node.op1) {
                            Some(offset) => offset.clone(),
                            _ => line.node.op1.clone(),
                        },
                        op2: Arg::Empty,
                        result: self.get_reg(&line.node.op1, &line.gen),
                    });
                }

                self.lines.extend(new_code);
                self.lines.push(node);
            }
            Command::CMPI | Command::CMPF => {
                let op1 = self.get_reg(&line.node.op1, &line.gen);
                let op2 = self.get_reg(&line.node.op2, &line.gen);

                let node = IRNode {
                    command: line.node.command.clone(),
                    op1: op1,
                    op2: op2,
                    result: Arg::Empty,
                };

                self.lines.extend(new_code);
                self.lines.push(node);
            }
            Command::POP => {
                if line.node.op1.is_reg() {
                    let node = IRNode {
                        command: line.node.command.clone(),
                        op1: line.node.op1.clone(),
                        op2: Arg::Empty,
                        result: Arg::Empty,
                    };

                    self.lines.push(node);
                } else {
                    let mut node = IRNode {
                        command: line.node.command.clone(),
                        op1: Arg::Empty,
                        op2: Arg::Empty,
                        result: Arg::Empty,
                    };

                    let (result, new_lines) = self.load(&line.node.op1, &line.gen, true);
                    new_code.extend(new_lines);

                    node.op1 = result;

                    self.lines.extend(new_code);
                    self.lines.push(node);
                }
            }
        }
    }

    /// Make sure you don't try to map an empty arg.
    fn get_reg(&mut self, arg: &Arg, gen_set: &HashSet<Arg>) -> Arg {
        let result = if *arg == Arg::Empty {
            // Ignore empties.
            Arg::Empty
        } else if let Some(entry) = self.used_regs.get(arg) {
            // Direct match found already loaded into a register.
            entry.reg.clone()
        } else if let Some(entry) = self.temp_map.get(arg) {
            if let Some(record) = self.used_regs.get(&entry) {
                // Corresonding offset is already in a register.
                record.reg.clone()
            } else {
                // Temp was spilled to an offset.
                entry.clone()
            }
        } else {
            // Named arg not in gen set.
            return arg.clone();
        };

        if result.is_offset() {
            // Temp was spilled to an offset. Load it back to reg.
            let (reg, lines) = self.load(&result, gen_set, false);
            self.lines.extend(lines);
            reg
        } else {
            result
        }
    }

    fn is_dirty(&self, arg: &Arg) -> bool {
        if let Some(entry) = self.temp_map.get(arg) {
            self.used_regs.get(&entry).unwrap().dirty.clone()
        } else if let Some(entry) = self.used_regs.get(arg) {
            entry.dirty.clone()
        } else {
            false
        }
    }

    fn ensure(&mut self, arg: &Arg, gen_set: &HashSet<Arg>) -> (Arg, Vec<IRNode>) {
        if arg.is_empty() || arg.is_reg() {
            return (Arg::Empty, Vec::new());
        }

        // If arg is a spilled temp, use its offset from
        // the function pointer instead.
        let arg_alias = if let Some(offset) = self.temp_map.get(arg) {
            // A reged temp.
            offset.clone()
        } else {
            // Not a temp.
            arg.clone()
        };

        if let Some(ref reg_record) = self.used_regs.get(&arg_alias) {
            return (reg_record.reg.clone(), Vec::new());
        }

        let (reg, mut code) = self.load(&arg_alias, gen_set, false);
        code.push(IRNode {
                      command: Command::STOREI,
                      op1: arg_alias.clone(),
                      op2: Arg::Empty,
                      result: reg.clone(),
                  });

        (reg, code)
    }

    fn load(&mut self, arg: &Arg, gen_set: &HashSet<Arg>, dirty: bool) -> (Arg, Vec<IRNode>) {
        // Make sure you map a spilled temp to its offset.
        let mut offset = Arg::Empty;
        let alias = if arg.is_temp() {
            match self.temp_map.get(arg) {
                Some(offset) => offset.clone(),
                None => {
                    offset = Arg::Offset(self.next_offset);
                    self.next_offset -= 1;

                    offset.clone()
                }
            }
        } else {
            arg.clone()
        };
        if arg.is_temp() {
            self.temp_map.insert(arg.clone(), offset);
        }

        if alias.is_empty() || alias.is_reg() {
            (alias.clone(), Vec::new())
        } else if self.free_regs.len() > 0 {
            // Get the free reg, push arg to the set of args in regs
            // and map the reg to the arg.
            let remove = self.free_regs.pop().unwrap();
            self.used_regs
                .insert(alias.clone(),
                        Reg::new(remove.clone(), alias.clone(), dirty));
            (remove, Vec::new())
        } else if let Some(lit) = self.used_regs
                      .iter()
                      .filter(|&(reg_arg, _)| {
                                  // pick a literal that isn't needed.
                                  reg_arg.is_literal() && !gen_set.contains(reg_arg)
                              })
                      .map(|(reg_arg, _)| reg_arg.clone())
                      .next() {

            // Spill a litteral.
            return (self.swap_state(&lit, &alias, dirty), Vec::new());
        } else if let Some(clean) = self.used_regs
                      .iter()
                      .filter(|&(reg_arg, reg)| {
                                  // pick a clean reg that isn't needed.
                                  reg.dirty == false && !gen_set.contains(reg_arg)
                              })
                      .map(|(reg_arg, _)| reg_arg.clone())
                      .next() {

            // Spill a clean register.
            return (self.swap_state(&clean, &alias, dirty), Vec::new());
        } else if let Some(some_var) = self.used_regs
                      .iter()
                      .filter(|&(reg_arg, _)| {
                                  // Pick a named arg or offset that isn't needed.
                                  (reg_arg.is_name() || reg_arg.is_offset()) &&
                                  !gen_set.contains(reg_arg)
                              })
                      .map(|(reg_arg, _)| reg_arg.clone())
                      .next() {

            // Spill a dirty register to a variable.
            let reg = self.swap_state(&some_var, &alias, dirty);
            let code = IRNode {
                command: Command::STOREI,
                op1: reg.clone(),
                op2: Arg::Empty,
                result: some_var,
            };

            (reg, vec![code])
        } else {
            // Spill a temp...
            let offset = Arg::Offset(self.next_offset);
            self.next_offset -= 1;

            let temp = self.used_regs
                .iter()
                .filter(|&(reg_arg, _)| !gen_set.contains(reg_arg))
                .map(|(reg_arg, _)| reg_arg.clone())
                .next()
                .unwrap();

            self.temp_map.insert(temp.clone(), offset.clone());
            let reg = if self.used_regs.contains_key(&offset) {
                self.swap_state(&offset, &alias, dirty)
            } else {
                self.swap_state(&temp, &alias, dirty)
            };
            let code = IRNode {
                command: Command::STOREI,
                op1: reg.clone(),
                op2: Arg::Empty,
                result: offset.clone(),
            };

            (reg, vec![code])
        }
    }

    /// Remove the old mapping and store the new value in the register.
    fn swap_state(&mut self, remove: &Arg, add: &Arg, dirty: bool) -> Arg {
        let Reg {
            arg: _,
            reg,
            dirty: _,
        } = self.used_regs.remove(remove).unwrap();
        self.used_regs
            .insert(add.clone(),
                    Reg {
                        arg: add.clone(),
                        reg: reg.clone(),
                        dirty: dirty,
                    });
        reg
    }
}
