/// COde GENerator Trees. Interface for Micro compiler written for Purdue ECE 46800 Fall 2017.
/// JWM
extern crate sparse;

use sparse::{SymbolTable, Program};

mod ast;
mod three_ac;
mod tiny_gen;
mod reg_alloc;

use std::collections::HashMap;

pub fn generate(symbols: HashMap<String, SymbolTable>, program: Program) -> String {
    let mut syn_tree = ast::AST::new(symbols.clone());
    syn_tree.construct(program);

    if syn_tree.error.is_none() {
        let mut code = three_ac::Code::new();
        code.construct(&syn_tree, &symbols);

        let mut allocator = reg_alloc::Allocator::new();
        allocator.construct(code);

        let mut tiny_code = tiny_gen::TinyCode::new();
        tiny_code.construct(&symbols, allocator);

        tiny_code.to_string()
    } else {
        format!("{:?}", syn_tree.error.unwrap())
    }
}
