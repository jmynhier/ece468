# JWM 24 August 2017
# Makefile for Purdue ECE468 Compiler project.

# Print formatted message for grading/identification.
.PHONEY: team
team:
	@echo Team: JM
	@echo 
	@echo Joseph Mynhier
	@echo jmynhier
	@echo 

# Remove build files.
.PHONEY: clean
clean: 
	cargo clean

# Build the compiler.
.PHONEY: compiler
compiler:
	cargo build
