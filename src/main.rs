#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]

/// Compiler written for Purdue ECE 46800 Fall 2017.
/// JWM

extern crate sparse;
extern crate cogent;

use std::env;
use std::fs::File;
use std::io::{Write, Read};
use sparse::parse::parse_program;
use cogent::generate;

fn main() {
    let args: Vec<_> = env::args().collect();
    if args.len() != 3 {
        eprintln!("ERROR: Did not get correct number of inputs!");
        return;
    }

    // Read the input file.
    let mut input: Vec<u8> = Vec::new();
    let mut file = match File::open(args[1].to_string()) {
        Ok(f) => f,
        Err(_) => {
            eprintln!("ERROR: Could not open input file!");
            return;
        }
    };

    if file.read_to_end(&mut input).is_err() {
        eprintln!("ERROR: Failed to read input file!");
        return;
    }

    // Print the token list.
    let mut output = match File::create(args[2].to_string()) {
        Ok(f) => f,
        _ => {
            eprintln!("ERROR: Count not open output file!");
            return;
        }
    };

    /*********** Step1 ****************************************
    // Scan the input.
    let tokens = match sparse::tokenize(&input[..]).to_result() {
        Ok(list) => list,
        _ => {
            eprintln!("ERROR: Tokenize failed!");
            return;
        }
    };

    for token in tokens.iter() {
        let _ = output.write(token.to_string().as_bytes());
    }
    *********** Step1 ****************************************/

    /********** Step2 *****************************************
    // parse the input.
    if parse_program(&input[..]).to_result().is_ok() {
        let _ = output.write("Accepted\n".as_bytes());
    } else {
        let _ = output.write("Not accepted\n".as_bytes());
    }
    *********** Step2 ****************************************/

    /********** Step3 *****************************************
    // Get the symbol table.
    if let Ok((_, symbol_table)) = parse_program(&input[..]).to_result() {
        let _ = match symbol_table {
            Ok(x) => output.write(format!("{:?}\n", x).as_bytes()),
            Err(y) => output.write(format!("{}\n", y).as_bytes()),
        };
    }
    *********** Step3 ****************************************/

    /********** Step4 ****************************************/
    match parse_program(&input[..]).to_result() {
        Ok((parse_tree, symbol_table_res)) => {
            match symbol_table_res {
                Ok(symbol_table) => {
                    let _ = output.write(format!("{}", generate(symbol_table, parse_tree)).as_bytes());
                }
                Err(symbol_err) => {
                    let _ = output.write(format!("{:?}\n", symbol_err).as_bytes());
                }
            }
        }
        Err(parse_err) => {
                let _ = output.write(format!("{:?}\n", parse_err).as_bytes());
        }
    }

    /********** Step4 ****************************************/
}
